//===========================================================================
//
//	Shader.fx
//	エフェクトファイル
//
//===========================================================================


//===========================================================================
//	グローバル変数
//===========================================================================
// ローカルから射影空間への座標変換
float4x4	mWVP;
// ローカルからワールドへの座標変換
float4x4	mWorld;

// ライトの位置
float4		vLightPos;
// ライトの方向
float4		vLightDir;

float		maxLength	= 10.0f;
float		attenuation = 0.7f;

float4		ambient = { 0.2f, 0.2f, 0.2f, 1.0f };
float4		diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
float		rate	= 1.0f;

float		alpha	= 1.0f;

//===========================================================================
//	テクスチャー
//===========================================================================
texture	Tex;
sampler Samp = sampler_state
{
	Texture		= <Tex>;
	MinFilter	= LINEAR;
	MagFilter	= LINEAR;
	MipFilter	= NONE;
	
	AddressU	= Wrap;
	AddressV	= Wrap;
};


//===========================================================================
//	頂点シェーダーからピクセルシェーダーに渡すデータ構造
//===========================================================================
struct	VS_OUTPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float3	normal	: NORMAL;
	float2	tex		: TEXCOORD0;
};

struct	VS_OUTPUTL
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float3	normal	: NORMAL;
	float2	tex		: TEXCOORD0;
	float4	world	: COLOR1;
};

struct	VS_INPUT
{
	float4	pos		: POSITION;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};

struct	VS_INPUTL
{
	float4	pos		: POSITION;
	float3	normal	: NORMAL;
	float4	color	: COLOR0;
	float2	tex		: TEXCOORD0;
};

//===========================================================================
//	ライティング
//===========================================================================
inline	float4	LightDir(float3 L, float3 N)
{
	return ambient + diffuse * rate * max(0, dot(L, N));
}

//===========================================================================
//	頂点シェーダー
//===========================================================================
// ライン用頂点シェーダー
VS_OUTPUT VS_Line(
	float4 pos		: POSITION,
	float4 color	: COLOR0
) {
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	// 座標設定
	Out.pos		= mul(pos, mWVP);
	// 頂点色設定
	Out.color = color;
	
	return Out;
}

// 基本頂点シェーダー(2D)
VS_OUTPUT VS_Basic2D( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	// 座標設定
	Out.pos	= In.pos;
	// 頂点色設定
	Out.color = In.color;
	// テクスチャー設定
	Out.tex = In.tex;

	return Out;
}

// 基本頂点シェーダー
VS_OUTPUT VS_Basic( VS_INPUT In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;

	// 座標設定
	Out.pos		= mul(In.pos, mWVP);
	// 頂点色セット
	Out.color	= In.color;
	// テクスチャー設定
	Out.tex		= In.tex;

	return Out;
}

// 平行光源
VS_OUTPUT VS_Light( VS_INPUTL In )
{
	VS_OUTPUT Out = (VS_OUTPUT)0;
	
	// ワールド座標設定
	Out.normal = mul(In.normal, mWorld);
	
	// 座標設定
	Out.pos		= mul(In.pos, mWVP);
	// テクスチャー設定
	Out.tex		= In.tex;
	// 頂点色セット
	Out.color	= In.color;
	
	return Out;
}

// 点光源
VS_OUTPUTL VS_PointLight( VS_INPUTL In )
{
	VS_OUTPUTL Out = (VS_OUTPUTL)0;
	
	// ワールド座標設定
	Out.world = mul(In.pos, mWorld);
	
	// 法線設定
	Out.normal = mul(In.normal, mWorld);
	
	// 座標設定
	Out.pos = mul(In.pos, mWVP);
	// テクスチャー設定
	Out.tex = In.tex;
	// 頂点色セット
	Out.color	= In.color;
	
	return Out;
}

//===========================================================================
// ピクセルシェーダー
//===========================================================================
// ライン用ピクセルシェーダー
float4	PS_Line( VS_OUTPUT In ) : COLOR
{
	return In.color;
}

// 基本ピクセルシェーダー
float4	PS_Basic( VS_OUTPUT In ) : COLOR
{
	return In.color * tex2D(Samp, In.tex);
}

float4	PS_Light( VS_OUTPUT In ) : COLOR
{
	// 頂点色の設定
	float3 L = normalize(-vLightDir.xyz);
	float3 N = normalize(In.normal);
	float4 color = In.color * LightDir(L, N);

	color = color * tex2D(Samp, In.tex);
	if( color.a > 0 ) color.a = alpha;

	return color;
}

float4 PS_PointLight( VS_OUTPUTL In ) : COLOR
{
	// 頂点色の設定
	float3 norL  = normalize(-vLightDir.xyz);
	float3 L = vLightPos.xyz - In.world.xyz;
	float lengthL = length(L);
	float4 color = 0;
	float3 N;
	if( lengthL < maxLength ) {
		L = normalize(L);
		N = normalize(In.normal);
		float light = (dot(L, N)) / (lengthL * lengthL * attenuation);
		color = max(0, light);
	}
	color = In.color * (color + LightDir(norL, N)) * tex2D(Samp, In.tex);
	if( color.a != 0 ) color.a = alpha;
	

	return color;
}

technique TShader
{
	pass P0
	{
		VertexShader = compile vs_3_0 VS_Line();
		PixelShader	 = compile ps_3_0 PS_Line();
	}
	pass P1
	{
		AlphaBlendEnable = true;
		BlendOp			 = Add;
		SrcBlend		 = SrcAlpha;
		DestBlend		 = InvSrcAlpha;
		ZWriteEnable	 = true;

		VertexShader = compile vs_3_0 VS_Line();
		PixelShader	 = compile ps_3_0 PS_Line();
	}
	pass P2
	{
		AlphaBlendEnable = true;
		BlendOp			 = Add;
		SrcBlend		 = SrcAlpha;
		DestBlend		 = InvSrcAlpha;
		ZWriteEnable	 = true;
		
		VertexShader = compile vs_3_0 VS_Basic2D();
		PixelShader	 = compile ps_3_0 PS_Basic();
	}
	pass P3
	{
		AlphaBlendEnable = true;
		BlendOp			 = Add;
		SrcBlend		 = SrcAlpha;
		DestBlend		 = InvSrcAlpha;
		ZWriteEnable	 = true;
		
		VertexShader = compile vs_3_0 VS_Basic();
		PixelShader	 = compile ps_3_0 PS_Basic();
	}
	pass P4
	{
		AlphaBlendEnable = true;
		BlendOp			 = Add;
		SrcBlend		 = SrcAlpha;
		DestBlend		 = InvSrcAlpha;
		ZWriteEnable	 = true;
		
		VertexShader = compile vs_3_0 VS_Light();
		PixelShader	 = compile ps_3_0 PS_Light();
	}
	pass P5
	{
		AlphaBlendEnable = true;
		BlendOp			 = Add;
		SrcBlend		 = SrcAlpha;
		DestBlend		 = InvSrcAlpha;
		ZWriteEnable	 = true;
		
		VertexShader = compile vs_3_0 VS_PointLight();
		PixelShader	 = compile ps_3_0 PS_PointLight();
	}
}

