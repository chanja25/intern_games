//===========================================================================
//!
//!	@file		SceneGame.cpp
//!	@brief		SceneGameクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
//	コンストラクタ

//---------------------------------------------------------------------------
SceneGame::SceneGame(void)
{
	for( u32 i=0; i<TEX_MAX; i++ ) {
		_pTexture[i] = NULL;
	}
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneGame::~SceneGame(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneGame::init(void)
{
	_pause = FALSE;
	_boss = TRUE;

	_playerNum = 1;

	//	使用するテクスチャーのロード
	_pTexture[0] = RESOURCE->loadTexture("Player.bmp");
	_pTexture[1] = RESOURCE->loadTexture("Player2.bmp");
	_pTexture[2] = RESOURCE->loadTexture("Enemy.bmp");
	_pTexture[3] = RESOURCE->loadTexture("Enemy2.bmp");
	_pTexture[4] = RESOURCE->loadTexture("Enemy3.bmp");
	_pTexture[5] = RESOURCE->loadTexture("Bullet.bmp");
	_pTexture[6] = RESOURCE->loadTexture("Bullet2.bmp");
	_pTexture[7] = RESOURCE->loadTexture("Item.bmp");
	_pTexture[8] = RESOURCE->loadTexture("floor.png");
	_pTexture[9] = RESOURCE->loadTexture("Bullet3.bmp");
	_pTexture[10] = RESOURCE->loadTexture("Boss.bmp");

	// ステージ描画用頂点データをセット
	DIRECTX->createVertex(&_pStage, 100.0f, 150.0f);

	// プレイヤーの初期化と追加
	TASK->addBottom(new Player(), SystemTask::LIST_PLAYER);

	// 敵の初期化と追加
	for( u32 i=0; i<27; i++ ) {
		TASK->addBottom(new Enemy(D3DXVECTOR3(-16.0f+(i/3*4.0f), 0, 35.0f-(i%3*4.0f)), 4), SystemTask::LIST_ENEMY);
	}

	// 敵2の初期化と追加
	for( u32 i=0; i<6; i++ ) {
		TASK->addBottom(new Enemy2(D3DXVECTOR3(-30.0f+(i*10.0f), 0, 45.0f), 10), SystemTask::LIST_ENEMY);
	}

	// 敵3の初期化と追加
	for( u32 i=0; i<40; i++ ) {
		TASK->addBottom(new Enemy3(D3DXVECTOR3(-10.0f+(i/4*2.0f), 0, 20.0f-(i%4*2.0f)), 2), SystemTask::LIST_ENEMY);
	}

	//	当たり判定の初期化
	COLLISION->init();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneGame::upDate(void)
{
	// 2Pのコントローラーでスタートが押されると2Pプレイヤーが追加される(一度だけ)
	if( (IInput->isButtonPush(1, XInput::BUTTON_START)) && (_playerNum < 2) ) {
		TASK->addBottom(new Player2(), SystemTask::LIST_PLAYER);
		_playerNum++;
	}

	// スタートボタンでポーズをする
	_pause = ( IInput->isButtonPush(0, XInput::BUTTON_START) )? !_pause: _pause;

	// デバッグ用文字表示
	if( _pause ) {
		FONT->drawFont( 0, 0, 0xffffffff, "PAUSE" );
		TASK->setAttribute(SystemTask::ATT_RENDER);
	} else {
		TASK->setAttribute(SystemTask::ATT_UPDATA | SystemTask::ATT_RENDER);
	}

	// 当たり判定の更新
	COLLISION->upDate();

	// ステージの描画
	D3DXMATRIX mat, rot, trans;
	D3DXMatrixTranslation(&trans, 0, 0, 50.0f);
	D3DXMatrixRotationX(&rot, D3DX_PI/2);
	mat = rot * trans;

	PRIMITIVE->drawTexture(D3DXVECTOR3(0, -5.0f, 5), &mat, _pStage, _pTexture[8]);

	// 敵とプレイヤーの情報を取得し、シーンの移行を行う
	if( TASK->getList(SystemTask::LIST_ENEMY)->getNum() == 0 ) {
		// 敵が全滅すると一度だけボスが出現する
		if( _boss ) {
			_boss = FALSE;
			TASK->addBottom(new Boss(D3DXVECTOR3(0, 0, 120), 600), SystemTask::LIST_ENEMY);
			return;
		}
		SCENE->jumpScene("SceneClear");
	} else if( TASK->getList(SystemTask::LIST_PLAYER)->getNum() == 0 ) {
		SCENE->jumpScene("SceneEnd");
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneGame::cleanUp(void)
{
	//	当たり判定の解放
	COLLISION->cleanUp();

	//	ステージの頂点情報解放
	SAFE_RELEASE(_pStage);

	//	テクスチャーの解放
	for( u32 i=0; i<TEX_MAX; i++ ) {
		RESOURCE->releaseTexture(_pTexture[i]);
	}

	TASK->setAttribute(SystemTask::ATT_UPDATA | SystemTask::ATT_RENDER);

	// タスクの解放
	TASK->killAll();
	
}
