//===========================================================================
//!
//!	@file		Profiler.cpp
//!	@brief		プロファイラ
//!
//===========================================================================
#include "SystemStdafx.h"

#if USE_PROFILER

//===========================================================================
// Timer
//===========================================================================

//---------------------------------------------------------------------------
// デフォルトコンストラクタ
//---------------------------------------------------------------------------
Timer::Timer() : _baseTime(0)
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);
	_baseFreq = li.QuadPart;

	reset();
}

//---------------------------------------------------------------------------
// 時刻の取得
//
//! @param なし
//! @retval 経過時刻、パフォーマンスカウンタが利用できない場合、0を返却します
//---------------------------------------------------------------------------
u32	Timer::get(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);

	if( _baseFreq ) {
		return (u32)(((li.QuadPart - _baseTime) * TICK_DELTA) / _baseFreq);
	} else {
		return 0;
	}
}

//---------------------------------------------------------------------------
// 経過時間のリセット
//
//! @param なし
//! @retval なし
//---------------------------------------------------------------------------
void Timer::reset(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	_baseTime = li.QuadPart;
}


//===========================================================================
// Profiler
//===========================================================================
// 唯一のインスタンス
Profiler* Profiler::_pInstance = NULL;

// 扱うログの数
const u32 Profiler::LOG_COUNT = 1024;

//---------------------------------------------------------------------------
// デフォルトコンストラクタ
//---------------------------------------------------------------------------
Profiler::Profiler(void)
: _pRootLog(NULL)
, _pCurrentLog(NULL)
, _logCount(0)
{
}

//---------------------------------------------------------------------------
// 初期化
//
//! @param なし
//! @retval true 初期化成功
//! @retval true 初期化失敗
//---------------------------------------------------------------------------
bool Profiler::init(void)
{
	// プロファイラ作成
	_pInstance = new Profiler();
	if( _pInstance == NULL ) {
		return false;
	}

	// ログ情報バッファ作成
	_pInstance->_pRootLog = new Log[LOG_COUNT];
	if( _pInstance->_pRootLog == NULL ) {
		return false;
	}
	memset(_pInstance->_pRootLog, 0, sizeof(Log) * LOG_COUNT);

	// ルートログを作成し、カレントにセット
	_pInstance->_pCurrentLog = _pInstance->createLog("Root", NULL);

	return true;
}

//---------------------------------------------------------------------------
// 終了
//
//! @param なし
//! @retval なし
//---------------------------------------------------------------------------
void Profiler::cleanup(void)
{
	// ログ情報バッファ解放
	if( _pInstance != NULL ) {
		delete [] _pInstance->_pRootLog;
	}

	// プロファイラ解放
	delete _pInstance;
}

//---------------------------------------------------------------------------
// インスタンスの取得
//
//! @param なし
//! @retval インスタンスのポインタ
//---------------------------------------------------------------------------
Profiler* Profiler::getInstancePtr(void)
{
	ASSERT( _pInstance != NULL, "インスタンスは取得できません" );
	return _pInstance;
}

//---------------------------------------------------------------------------
// フレームの開始をプロファイラに伝える
//
//! @param なし
//! @retval なし
//---------------------------------------------------------------------------
void Profiler::startFrame(void)
{
	Log* pLog = _pRootLog;

	// 計測しているログ情報があればログ情報を更新
	if( pLog->_pChild ) {
		updateLog(pLog->_pChild);
	}

	// タイマーリセット
	_timer.reset();
}

//---------------------------------------------------------------------------
// 計測開始
//
//! @param name [in] プロファイル名
//! @param color [in] 表示色
//! @retval なし
//---------------------------------------------------------------------------
void Profiler::begin(const char* pName, Profiler::COLOR color)
{
	Log* pParent  = _pCurrentLog;
	Log* pBrother = NULL;		// 兄

	// 指定プロファイル名を持つ既存ログを探す
	Log* pLog = pParent->_pChild;
	while( pLog ) {
		if( pLog->_pName == pName ) {
			break;
		}
		pBrother = pLog;		// 兄を記憶
		pLog = pLog->_pNext;
	}

	if( pLog == NULL ) {

		// 指定プロファイルが存在しなかったので、新規ログ作成
		pLog = createLog(pName, pParent);

		// カラー設定
		pLog->_color = color;

		// 親兄弟関係設定
		if( pParent->_pChild == NULL ) {
			// 子供がいなかったのなら、第一子に
			pParent->_pChild = pLog;
		} else {
			// 既に子供がいたのなら、兄にリンクされて、第二子以降になる
			pBrother->_pNext = pLog;
		}

	}

	// 計測開始時間取得
	pLog->_beginTime = _timer.get();

	// カレントログとしてセット
	_pCurrentLog = pLog;
}

//---------------------------------------------------------------------------
// 計測終了
//
//! @param なし
//! @retval なし
//---------------------------------------------------------------------------
void Profiler::end(const char* pName)
{
	ASSERT( _pCurrentLog->_pName == pName, "計測開始と計測終了が対になっていません" );

	// 計測終了時間取得
	_pCurrentLog->_endTime = _timer.get();

	// 経過時間取得
	u32 time = _pCurrentLog->_endTime - _pCurrentLog->_beginTime;

	// 合計処理時間加算
	_pCurrentLog->_tmpTotalTime += time;
	// 計測回数インクリメント
	_pCurrentLog->_tmpCount++;
	// 最小処理時間更新
	_pCurrentLog->_tmpMinTime = (_pCurrentLog->_tmpMinTime > time) ? time : _pCurrentLog->_tmpMinTime;
	// 最大処理時間更新
	_pCurrentLog->_tmpMaxTime = (_pCurrentLog->_tmpMaxTime < time) ? time : _pCurrentLog->_tmpMaxTime;

	// 計測終了したので、カレントを親に設定しなおす
	_pCurrentLog = _pCurrentLog->_pParent;
}

//---------------------------------------------------------------------------
// ルートログの取得
//
//! @param なし
//! @retval ルートログ
//---------------------------------------------------------------------------
Profiler::Log* Profiler::getRootLog(void)
{
	return _pRootLog;
}

//---------------------------------------------------------------------------
// ログ情報の生成
//
//!	@param	name    [in] プロファイル名
//!	@param	pParent [in] 親となるログ情報のポインタ
//! @retval 新規ログ情報のポインタ
//---------------------------------------------------------------------------
Profiler::Log* Profiler::createLog(const char* pName, Log* pParent)
{
	ASSERT( _logCount + 1 < LOG_COUNT, "これ以上ログを作成できません" );

	Log* pLog = &_pRootLog[_logCount];
	_logCount++;

	pLog->_pParent	= pParent;
	pLog->_pChild	= NULL;
	pLog->_pNext	= NULL;

	// 計測用パラメータ初期化
	pLog->_beginTime		= 0;
	pLog->_endTime			= 0;
	pLog->_tmpCount			= 0;
	pLog->_tmpTotalTime		= 0;
	pLog->_tmpMinTime		= (u32)-1;
	pLog->_tmpMaxTime		= 0;

	// 表示用パラメータ初期化
	pLog->_pName			= pName;
	pLog->_color			= pParent ? pParent->_color : COLOR_WHITE;
	pLog->_count			= 0;
	pLog->_totalTime		= 0;
	pLog->_averageTime		= 0;
	pLog->_minTime			= (u32)-1;
	pLog->_maxTime			= 0;

	return pLog;
}

//---------------------------------------------------------------------------
// ログ情報の更新
//
//!	@param	pLog [in/out] 更新するログ情報の第一子
//! @retval なし
//---------------------------------------------------------------------------
void Profiler::updateLog(Profiler::Log* pLog)
{
	while( pLog != NULL ) {

		pLog->_count			= pLog->_tmpCount;
		pLog->_totalTime		= pLog->_tmpTotalTime;
		pLog->_averageTime		= pLog->_count != 0 ? (pLog->_totalTime / pLog->_count) : 0;
		pLog->_minTime			= pLog->_tmpMinTime;
		pLog->_maxTime			= pLog->_tmpMaxTime;

		pLog->_beginTime		= 0;
		pLog->_endTime			= 0;
		pLog->_tmpCount			= 0;
		pLog->_tmpTotalTime		= 0;

		// 子がNULLでなければ再帰的に更新処理を行う
		if( pLog->_pChild ) {
			updateLog(pLog->_pChild);
		}

		pLog = pLog->_pNext;

	}
}


//===========================================================================
//	ScopeProfiler
//===========================================================================
//---------------------------------------------------------------------------
//	コンストラクタ
//
//!	@param name [in] プロファイル名
//! @param color [in] 表示色
//---------------------------------------------------------------------------
ScopeProfiler::ScopeProfiler(const char* pName, Profiler::COLOR color)
: _pName(pName)
{
	Profiler::getInstancePtr()->begin(_pName, color);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
ScopeProfiler::~ScopeProfiler(void)
{
	Profiler::getInstancePtr()->end(_pName);
}


#endif //~#if USE_PROFILER

//===========================================================================
//	END OF FILE
//===========================================================================
