//===========================================================================
//!
//!	@file		SystemScene.h
//!	@brief		SystemSceneクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// SystemSceneクラス
//===========================================================================
class	SystemScene : public SceneBase
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! 初期化
	void	init(void);
	//! 更新
	void	upDate(void);
	//! 解放
	void	cleanUp(void);
	//!@}

	//! シーン変更
	void	jumpScene(LPSTR scene);

	//! 現在のシーンの取得
	SceneBase*	getCurrentScene(void);

	//! インスタンス取得関数
	static	SystemScene*	getInstance(void);

private:
	//! コンストラクタ
	SystemScene(void);
	//! デストラクタ
	~SystemScene(void);

	static		SystemScene _instance;	//!< 唯一のインスタンス

	SceneBase*	_pCurrentScene;			//!< 現在のシーン
};


//! システムシーンの取得関数のマクロ
#define	SCENE	(SystemScene::getInstance())