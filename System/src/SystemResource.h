//===========================================================================
//!
//!	@file		SystemResource.h
//!	@brief		SystemResourceクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// SystemResourceクラス
//===========================================================================
class	SystemResource
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! 初期化
	void init(void);
	//! 更新
	void upDate(void);
	//! 解放
	void cleanUp(void);
	//!@}

	//-------------------------------------------------------------
	//!	@name テクスチャー管理
	//-------------------------------------------------------------
	//!@{
	//!	テクスチャーの読み込み
	LPDIRECT3DTEXTURE9	loadTexture(LPSTR pFileName);
	//!	テクスチャーの解放
	void				releaseTexture(LPDIRECT3DTEXTURE9 pTexture);
	//!@}

	//!	インスタンスの取得
	static	SystemResource*		getInstance(void);

private:
	//!	コンストラクタ
	SystemResource(void);
	//!	デストラクタ
	~SystemResource(void);

	static SystemResource	_instance;	//!< 唯一のインスタンス


	static const u8 MAX_INFORMATION = 128;	//!< テクスチャーの最大管理数
	//!	リソース情報格納構造体
	struct INFORMATION {
		char	_name[64];	//!< ファイル名
		u32		_count;		//!< ファイルの使用数

		LPDIRECT3DTEXTURE9	_pTexture; //!< テクスチャーへのポインタ
	};


	INFORMATION	_info[MAX_INFORMATION];		//!< リソース情報管理構造体
};

#define	RESOURCE	(SystemResource::getInstance())