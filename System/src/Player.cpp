//===========================================================================
//!
//!	@file		Player.cpp
//!	@brief		Playerクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Player::Player(void)
{
}


//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Player::~Player(void)
{
}


//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Player::init(void)
{
	_pTexture = RESOURCE->loadTexture("Player.bmp");

	// プレイヤーの初期設定
	_pos.x = 0;
	_pos.y = 0;
	_pos.z = 1;

	_life = 10;

	_bulletLevel = 0;
	_damage = FALSE;

	_node.setObject(this);
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_PLAYER );

	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Player::upDate(void)
{
	//	デバッグ用敵排除処理
	if( IInput->isButtonPush(0, XInput::BUTTON_RIGHT_SHOULDER) ) {
		TASK->getList(SystemTask::LIST_ENEMY)->allDeleteNode();
	}

	//---------------------------------------------------------------------------
	// ボタンの入力で移動する
	//---------------------------------------------------------------------------
	D3DXVECTOR2 thumb = IInput->getLeftThumb(0);

	_pos.z += .13f * thumb.y;
	if( _pos.z < 1.0f ) _pos.z = 1.0f;
	if( _pos.z > 9.0f ) _pos.z = 9.0f;
	_pos.x += .13f * thumb.x;
	if( _pos.x < -14.0f ) _pos.x = -14.0f;
	if( _pos.x >  14.0f ) _pos.x =  14.0f;

	//	Aボタンを押すと弾を発射する
	if( IInput->isButtonPush(0, XInput::BUTTON_A) ) {
		if( _bulletLevel == 0 ) {
			Bullet* pBullet = new Bullet();
			pBullet->setState(_pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		} else if( _bulletLevel == 1 ){
			D3DXVECTOR3 pos = _pos;
			pos.x -= 0.5f;
			Bullet* pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 1.0f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		} else {
			D3DXVECTOR3 pos = _pos;
			pos.x -= 0.5f;
			Bullet* pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(-0.2f, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 0.5f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 0.5f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0.2f, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		}
	}

	_alpha = 1.0f;
	_damage = FALSE;

	// 攻撃を受けていればライフを減らす
	u32 state = _object.getState();
	if( state ) {
		if( state == SystemCollision::Object::ATT_ENEMY_BULLET ) {
			_life--;
			_alpha = 0.1f;
			_damage = TRUE;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_ENEMY_BULLET2 ){
			_life -= 2;
			_alpha = 0.1f;
			_damage = TRUE;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_BOSS_BULLET ){
			_life -= 5;
			_alpha = 0.1f;
			_damage = TRUE;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_ITEM ){
			if( _bulletLevel < 2 ) {
				_bulletLevel++;
			} else {
				_life += 2;
				if( _life > 10 ) _life = 10;
			}
		}
	}

	// ライフが0なら削除要請を出す。そうでなければ当たり判定をセットする
	if( _life <= 0 ) {
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 1.0f);
		COLLISION->setObject(&_object);
	}

	// カメラの位置をセットする
	D3DXVECTOR3 pos = _pos;
	if( _damage ) {
		pos.z += 5.0f;
		pos.y += 0.5f;
		CAMERA->setTarget(pos);
		pos.z -= 15.0f;
		pos.y += 5.0f;
		CAMERA->setPos(pos);
	} else {
		pos.z += 5.0f;
		CAMERA->setTarget(pos);
		pos.z -= 15.0f;
		pos.y += 5.0f;
		CAMERA->setPos(pos);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Player::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Player::render(void)
{
	// ライフゲージの描画
	f32 gauge = _life / 10.0f;
	PRIMITIVE->drawRect(10, 410, 100*gauge, 20, 0xff0000ff);

	// 球の描画
	PRIMITIVE->drawSphere(_pos, 1.0f, 0xffff0000, 8);
	PRIMITIVE->drawSphere(_pos, 1.0f, _pTexture, 15, _alpha);
}
