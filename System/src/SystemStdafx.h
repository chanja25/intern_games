//===========================================================================
//!
//!	@file		SystemStdafx.h
//!	@brief		プリコンパイルヘッダ
//!
//===========================================================================
#pragma once

// 下で指定された定義の前に対象プラットフォームを指定しなければならない場合、以下の定義を変更してください。
// 異なるプラットフォームに対応する値に関する最新情報については、MSDN を参照してください。
#ifndef WINVER				// Windows XP 以降のバージョンに固有の機能の使用を許可します。
#define WINVER 0x0501		// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_WINNT		// Windows XP 以降のバージョンに固有の機能の使用を許可します。                   
#define _WIN32_WINNT 0x0501	// これを Windows の他のバージョン向けに適切な値に変更してください。
#endif						

#ifndef _WIN32_WINDOWS		// Windows 98 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_WINDOWS 0x0410 // これを Windows Me またはそれ以降のバージョン向けに適切な値に変更してください。
#endif

#ifndef _WIN32_IE			// IE 6.0 以降のバージョンに固有の機能の使用を許可します。
#define _WIN32_IE 0x0600	// これを IE. の他のバージョン向けに適切な値に変更してください。
#endif

#define WIN32_LEAN_AND_MEAN		// Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル:
#include <windows.h>

// C ランタイム ヘッダー ファイル
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <assert.h>
#include <stdio.h>
#include <time.h>

// 4996番の警告を非表示にする
#pragma warning (disable:4996)


// DirectX ヘッダー
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9math.h>

// DirectX Lib
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#pragma comment(lib, "winmm.lib")

// 解放用マクロ
#define	SAFE_RELEASE(__p__)		if( __p__ ){ __p__->Release(); __p__ = NULL; }
#define SAFE_DELETE(__p__)		if( __p__ ){ delete __p__; __p__ = NULL; }
#define SAFE_DELETES(__p__)		if( __p__ ){ delete [] __p__; __p__ = NULL; }

// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
#include "typedef.h"
#include "Shader.h"
#include "DirectX.h"
#include "SystemResource.h"
#include "DebugCamera.h"
#include "SystemCamera.h"
#include "Input.h"
#include "Font.h"
#include "Node.h"
#include "List.h"
#include "TaskBase.h"
#include "SystemTask.h"
#include "SystemCollision.h"
#include "SystemPrimitive.h"
#include "SceneBase.h"
#include "System.h"
#include "Character.h"
#include "SystemScene.h"

#include "Player.h"
#include "Player2.h"
#include "Enemy.h"
#include "Enemy2.h"
#include "Enemy3.h"
#include "Boss.h"
#include "Bullet.h"
#include "Bullet2.h"
#include "BossBullet.h"
#include "Item.h"
#include "SceneDemo.h"
#include "SceneTitle.h"
#include "SceneGame.h"
#include "SceneEnd.h"
#include "SceneClear.h"

#include "Profiler.h"

//===========================================================================
//	END OF FILE
//===========================================================================
