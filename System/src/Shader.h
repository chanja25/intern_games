//===========================================================================
//!
//!	@file		Shader.h
//!	@brief		Shaderクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// Shaderクラス
//===========================================================================
class Shader
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//!	コンストラクタ
	Shader(void);
	//!	デストラクタ
	~Shader(void);

	//!	初期化
	b32		init(LPSTR pFileName);
	//!	解放
	void	cleanUp(void);
	//!@}

	//!	エフェクトの取得
	LPD3DXEFFECT	getEffect(void){ return _pEffect; };

	//!	シェーダーの描画開始
	void	beginShader(void);
	//!	シェーダーの描画終了
	void	endShader(void);

	//!	シェーダーのパス
	enum PASS {
		PASS_LINE,
		PASS_2D_RECT,
		PASS_2D,
		PASS_3D,
		PASS_3D_LIGHT,
		PASS_3D_POINTLIGHT,
	};

	//-------------------------------------------------------------
	//!	@name 取得、設定
	//-------------------------------------------------------------
	//!@{
	//!	使用するテクニックのセット
	void	setTechnique(u32 technique);
	//!	使用するパスのセット
	void	setPass(u32 pass) { _pass = pass; };
	//!	テクスチャーの設定
	void	setTexture(LPDIRECT3DTEXTURE9 pTexture);
	//!	マトリックスの設定
	void	setMatrix(const D3DXMATRIX& world);
	//!	ライトの向き設定
	void	setLightDir(const D3DXVECTOR3& lightDir);
	//!	ライトの向き取得
	D3DXVECTOR3	getLightDir(void) { return D3DXVECTOR3(_light.x, _light.y, _light.z) ; };
	//!	ライトの位置設定
	void	setLightPos(const D3DXVECTOR3& lightPos);
	//!	アルファー値
	void	setAlpha(f32 alpha);
	//!@}

private:

	LPD3DXEFFECT	_pEffect;	//!< エフェクト
	D3DXHANDLE		_technique;	//!< 使用するテクニック
	D3DXHANDLE		_mWVP;		//!< ローカル〜射影行列	
	D3DXHANDLE		_mWorld;	//!< ローカル〜射影行列	
	D3DXHANDLE		_lightDir;	//!< ライトの向き
	D3DXHANDLE		_lightPos;	//!< ライトの位置
	D3DXHANDLE		_texture;	//!< テクスチャー
	D3DXHANDLE		_alpha;		//!< アルファ値

	D3DXVECTOR4		_light;			//!< ライトの向き
	D3DXVECTOR4		_lightPosition;	//!< ライトの位置

	u32				_pass;		//!< 使用するパス
};
