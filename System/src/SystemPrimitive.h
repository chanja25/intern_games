//===========================================================================
//!
//!	@file		SystemPrimitive.h
//!	@brief		SystemPrimitiveクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// SystemPrimitiveクラス
//===========================================================================
class SystemPrimitive
{
public:
	//-------------------------------------------------------------
	//!	@name 頂点フォーマット宣言
	//-------------------------------------------------------------
	//!@{
	//! フレキシブル頂点フォーマットによる頂点宣言
	static const DWORD D3DFVF_LINEVERTEX = D3DFVF_XYZ | D3DFVF_DIFFUSE;
	//! LINE頂点バッファ構造体
	struct LINEVERTEX
	{
		f32	x, y, z;
		DWORD	color;
	};

	//! フレキシブル頂点フォーマットによる頂点宣言
	static const DWORD D3DFVF_CUSTOMVERTEX = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1;
	//! カスタム頂点バッファ構造体
	struct CUSTOMVERTEX
	{
		f32		x, y, z;
		f32		nx, ny, nz;
		DWORD	color;
		f32		tu, tv;
	};

	//! フレキシブル頂点フォーマットによる頂点宣言(2D)
	static const DWORD D3DFVF_CUSTOMVERTEX2D = D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1;
	//! CUSTOM頂点バッファ構造体(2D)
	struct CUSTOMVERTEX2D 
	{
		f32	x, y, z, rhw;
		DWORD	color;
		f32 tu, tv;
	};

	//! フレキシブル頂点フォーマットによる頂点宣言(2D)
	static const DWORD D3DFVF_VERTEX2D = D3DFVF_XYZRHW | D3DFVF_DIFFUSE;
	//! 頂点バッファ構造体(2D)
	struct VERTEX2D 
	{
		f32	x, y, z, rhw;
		DWORD	color;
	};
	//!@}

	static	const u32	MAX_VERTEXBUFFER = 2048;
	static	const u32	MAX_TRIANGLEBUFFER = 20000;

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//!	初期化
	void	init(void);
	//!	解放
	void	cleanUp(void);
	//!@}

	//-------------------------------------------------------------
	//!	@name 描画関連
	//-------------------------------------------------------------
	//!@{
	//!	描画
	void	render(void);
	//!	ポリゴンの描画
	void	renderLine(void);
	//!	テクスチャーの描画
	HRESULT	drawTexture(const D3DXVECTOR3& pos, const D3DXMATRIX* mat, const LPDIRECT3DVERTEXBUFFER9 pVB, LPDIRECT3DTEXTURE9 pTexture, f32 alpha=1.0f);
	//!	テクスチャーの描画
	HRESULT	drawTexture(f32 x, f32 y, f32 width, f32 height, const LPDIRECT3DTEXTURE9 pTexture, DWORD color);
	//! 2D面の表示
	HRESULT	drawRect(f32 x, f32 y, f32 width, f32 height, DWORD color);
	//!	面の描画
	HRESULT	drawTriangle(const D3DXVECTOR3& pos, LPDIRECT3DTEXTURE9 pTexture, f32 alpha=1.0f);

	//!	頂点情報を追加
	void	setVertex(LINEVERTEX* pVertex, u32 size);
	//!	頂点情報を追加
	void	setTriangle(CUSTOMVERTEX *pVertex);
	//!	矢印の描画要請
	void	drawArrow(const D3DXVECTOR3& pos, const D3DXVECTOR3& vec, DWORD color);
	//!	スフィアの描画要請
	void	drawSphere(const D3DXVECTOR3& pos, f32 radius, DWORD color, u32 divideCount);
	//!	カプセルの描画要請
	void	drawCapsule(const D3DXVECTOR3& pos1, const D3DXVECTOR3& pos2, f32 radius, DWORD color, u32 divideCount);
	//!	スフィアの描画要請
	void	drawSphere(const D3DXVECTOR3& pos, f32 radius, LPDIRECT3DTEXTURE9 pTexture, u32 divideCount,f32 alpha=1.0f);
	//!@}

	//!	シェーダーの取得
	//!	@retval	シェーダー
	Shader*		getShader(void) { return &_shader; };

	//!	インスタンスの取得
	static	SystemPrimitive*	getInstance();

private:
	//!	コンストラクタ
	SystemPrimitive(void);
	//!	デストラクタ
	~SystemPrimitive(void);

	//!	デストラクタ
	static	SystemPrimitive _instance;	//!< 唯一のインスタンス

	u32				_vertexNum;		//!< 頂点の数
	u32				_triangleNum;	//!< 頂点の数
	u32				_triangleNo;	//!< 頂点の数

	LINEVERTEX*		_pVertex;		//!< 表示する頂点情報
	CUSTOMVERTEX*	_pTriangle;		//!< 表示する頂点情報
	
	Shader			_shader;		//!< シェーダー
};

//! システムプリミティブの取得関数のマクロ
#define	PRIMITIVE	(SystemPrimitive::getInstance())