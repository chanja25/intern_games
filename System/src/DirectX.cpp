//===========================================================================
//!
//!	@file		DirectX.cpp
//!	@brief		DirectXクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	インスタンスの生成
//---------------------------------------------------------------------------
DirectX	DirectX::_instance;

//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
DirectX::DirectX(void) :
_pDevice(NULL),
_pD3DDevice(NULL)
{
}

//---------------------------------------------------------------------------
//	初期化
//! @param	hWnd [in] 初期化用ウインドウハンドル
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	DirectX::init(HWND hWnd)
{
	//---------------------------------------------------------------------------
	// Direct3D9デバイスの作成
	//---------------------------------------------------------------------------
	if( NULL == (_pDevice = Direct3DCreate9( D3D_SDK_VERSION )) ) {
		return E_FAIL;
	}

	// Direct3D9デバイスにディスプレイのモードを設定
	D3DDISPLAYMODE d3ddm;
	if( FAILED( _pDevice->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm) ) ) {
		return E_FAIL;
	}

	//---------------------------------------------------------------------------
	// Direct3Dデバイスの作成
	//---------------------------------------------------------------------------
	ZeroMemory(&_d3dpp, sizeof(_d3dpp));
	_d3dpp.Windowed = TRUE;
	_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	_d3dpp.BackBufferFormat = d3ddm.Format;
	_d3dpp.EnableAutoDepthStencil = TRUE;
	_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// 描画と頂点処理をハードウェアで行う
	if( FAILED(_pDevice->CreateDevice(	D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
										D3DCREATE_HARDWARE_VERTEXPROCESSING,
										&_d3dpp, &_pD3DDevice )) ) {
		// 描画と頂点処理をハードウェアとソフトウェアの両方で行う
		if( FAILED(_pDevice->CreateDevice(	D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
											D3DCREATE_MIXED_VERTEXPROCESSING,
											&_d3dpp, &_pD3DDevice )) ) {
			// 描画をハードウェアで行い、頂点処理はCPUで行う
			if( FAILED(_pDevice->CreateDevice(	D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
												D3DCREATE_SOFTWARE_VERTEXPROCESSING,
												&_d3dpp, &_pD3DDevice ) ) ) {
				// 描画と頂点処理をCPUで行う
				if( FAILED(_pDevice->CreateDevice(	D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd,
													D3DCREATE_SOFTWARE_VERTEXPROCESSING,
													&_d3dpp, &_pD3DDevice ) ) ) {
					return E_FAIL;
				}
			}
		}
	}

	// デバイスの能力を確認
	D3DCAPS9 caps;
	_pDevice->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);

	if( caps.VertexShaderVersion < D3DVS_VERSION(2, 0) ) {
		MessageBox(NULL, "頂点シェーダーver2.0に対応していません", "Shader", MB_OK);
	}
	if( caps.PixelShaderVersion < D3DPS_VERSION(2, 0) ) {
		MessageBox(NULL, "ピクセルシェーダーver2.0に対応していません", "Shader", MB_OK);
	}

	// レンダーステータスの設定
	//_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	return	S_OK;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	DirectX::cleanUp(void)
{
	// Direct3Dデバイスの解放
	SAFE_RELEASE(_pD3DDevice);

	// Direct3D9デバイスの解放
	SAFE_RELEASE(_pDevice);
}

//---------------------------------------------------------------------------
//	描画開始
//---------------------------------------------------------------------------
void	DirectX::beginScene(void)
{
	// ディスプレイのクリア
	_pD3DDevice->Clear( 0L,
						NULL,
						D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
						D3DCOLOR_RGBA( 0, 0, 0, 255 ),
						1.0f,
						0 );
	// 描画開始
	_pD3DDevice->BeginScene();
}

//---------------------------------------------------------------------------
//	描画終了
//---------------------------------------------------------------------------
void	DirectX::endScene(void)
{
	// 描画終了
	_pD3DDevice->EndScene();

	// 描画したものをウインドウへ反映させる
	if( _pD3DDevice->Present(NULL, NULL, NULL, NULL) ) {
		_pD3DDevice->Reset(&_d3dpp);
	}
}

//---------------------------------------------------------------------------
//	頂点情報の初期化
//! @retval	Direct3Dデバイス
//---------------------------------------------------------------------------
LPDIRECT3DDEVICE9 DirectX::getD3DDevice(void)
{
	return _pD3DDevice;
}

//---------------------------------------------------------------------------
//	頂点情報作成
//!	@param ppVB		[in] 頂点情報を入れる
//! @param	width	[in] 表示する横幅
//! @param	height	[in] 表示する縦幅
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	DirectX::createVertex(LPDIRECT3DVERTEXBUFFER9* ppVB, f32 width, f32 height)
{
	// 頂点情報の設定
	SystemPrimitive::CUSTOMVERTEX vertices[4];

	vertices[0].x = vertices[2].x = -width/2;
	vertices[1].x = vertices[3].x = width/2;

	vertices[0].y = vertices[1].y = -height/2;
	vertices[2].y = vertices[3].y = height/2;
	
	vertices[0].nx = 0.0f;
	vertices[0].ny = 0.0f;
	vertices[0].nz = 1.0f;

	vertices[1].nx = 0.0f;
	vertices[1].ny = 0.0f;
	vertices[1].nz = 1.0f;

	vertices[2].nx = 0.0f;
	vertices[2].ny = 0.0f;
	vertices[2].nz = 1.0f;

	vertices[3].nx = 0.0f;
	vertices[3].ny = 0.0f;
	vertices[3].nz = 1.0f;

	vertices[0].tu = vertices[2].tu = 1.0;
	vertices[1].tu = vertices[3].tu = 0.0f;

	vertices[0].tv = vertices[1].tv = 0;
	vertices[2].tv = vertices[3].tv = -1.0f;

	vertices[0].z = vertices[1].z = vertices[2].z = vertices[3].z = 0;
	vertices[0].color = vertices[1].color = vertices[2].color = vertices[3].color = 0xffffffff;


	//---------------------------------------------------------------------------
	// 頂点バッファの設定
	//---------------------------------------------------------------------------
	if( FAILED( _pD3DDevice->CreateVertexBuffer(4*sizeof(SystemPrimitive::CUSTOMVERTEX),
												0, SystemPrimitive::D3DFVF_CUSTOMVERTEX,
												D3DPOOL_DEFAULT, ppVB, NULL) ) ) {
		return E_FAIL;
	}

	// 頂点情報を入れる
	VOID*	pVertices;
	if( FAILED( (*ppVB)->Lock(0, sizeof(vertices), (void**)&pVertices, 0) ) ) {
		return E_FAIL;
	}
	memcpy(pVertices, vertices, sizeof(vertices));
	(*ppVB)->Unlock();

	return S_OK;
}

//---------------------------------------------------------------------------
//	任意軸回転行列の作成
//!	@param	out		[out] 出力するポインタ
//!	@param	axis	[in]  回転させる軸
//!	@param	angle	[in]  回転量
//---------------------------------------------------------------------------
void		DirectX::axisRotationMatrix(D3DXMATRIX* out, const D3DXVECTOR3& axis, f32 angle)
{
	out->_11 = axis.x * axis.x * (1-cosf(angle)) + cosf(angle);
	out->_12 = axis.x * axis.y * (1-cosf(angle)) - axis.z * sinf(angle);
	out->_13 = axis.x * axis.z * (1-cosf(angle)) + axis.y * sinf(angle);
	out->_14 = 1;

	out->_21 = axis.y * axis.x * (1-cosf(angle)) + axis.z * sinf(angle);
	out->_22 = axis.y * axis.y * (1-cosf(angle)) + cosf(angle);
	out->_23 = axis.y * axis.z * (1-cosf(angle)) - axis.x * sinf(angle);
	out->_24 = 1;

	out->_31 = axis.z * axis.x * (1-cosf(angle)) - axis.y * sinf(angle);
	out->_32 = axis.z * axis.y * (1-cosf(angle)) + axis.x * sinf(angle);
	out->_33 = axis.z * axis.z * (1-cosf(angle)) + cosf(angle);
	out->_34 = 1;

	out->_41 = 0;
	out->_42 = 0;
	out->_43 = 0;
	out->_44 = 1;
}

//---------------------------------------------------------------------------
//	インスタンスの取得
//! @retval インスタンス
//---------------------------------------------------------------------------
DirectX*	DirectX::getInstance(void)
{
	return &_instance;
}
