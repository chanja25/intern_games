//===========================================================================
//!
//!	@file		Boss.cpp
//!	@brief		Bossクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Boss::Boss(void)
{
}

//---------------------------------------------------------------------------
//	引数付きコンストラクタ
//!	@param pos	[in] 初期位置
//!	@param life	[in] 体力
//---------------------------------------------------------------------------
Boss::Boss(D3DXVECTOR3 pos, s32 life)
{
	_pos = pos;
	_life = life;
}

//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Boss::~Boss(void)
{
}

//---------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------
void	Boss::init(void)
{
	_pTexture = RESOURCE->loadTexture("Boss.bmp");

	// 敵の初期設定
	_shootCount  = (rand()%10+10) * 20;
	_moveCount	 = 90;
	_countSpeed = 1;

	// ノードの設定
	_node.setObject(this);

	// 当たり判定用オブジェクトの初期化
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_BOSS );

	// タスクの設定
	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Boss::upDate(void)
{
	_alpha = 1.0f;

	// 弾に当たればライフを減らす
	u32 state = _object.getState();
	if( state == SystemCollision::Object::ATT_BOSS_BULLET ) {
		_life -= 60;
		_alpha = 0.1f;
	}
	if( state == SystemCollision::Object::ATT_PLAYER_BULLET ) {
		_life --;
	}

	// 画面に移る範囲まで出てくる
	if( _pos.z > 50 ) {
		_pos.z -= 1.5f;
	}

	//	移動
	if( _life < 240 ) {
		if( _pos.z > 40 ) {
			_pos.z -= 0.4f;
		}
		_pos.z += sinf(_moveCount*(D3DX_PI/90)) * 0.3f;
	}
	_pos.x += cosf(_moveCount*(D3DX_PI/90)) * 0.5f;
	_moveCount += _countSpeed;

	// 弾の制御
	if( _shootCount-- == 0 ) {
		_shootCount = 80;
		BossBullet* pBullet = new BossBullet();
		pBullet->setState(_pos, D3DXVECTOR3(0, 0, -1), SystemCollision::Object::ATT_BOSS_BULLET, SystemCollision::Object::ATT_PLAYER);
		TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
	}

	// ライフが無くなれば削除要請、あれば当たり判定をつける
	if( _life <= 0 ) {
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 5.0f);
		COLLISION->setObject(&_object);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Boss::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Boss::render(void)
{
	// ライフゲージの描画
	f32 gauge = _life / 600.0f;
	PRIMITIVE->drawRect(10, 40, 200*gauge, 20, 0xffff0000);

	// 球の描画
	PRIMITIVE->drawSphere(_pos, 5.0f, _pTexture, 20, _alpha);
}




