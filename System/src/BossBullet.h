//===========================================================================
//!
//!	@file		BossBullet.h
//!	@brief		BossBulletクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// BossBulletクラス
//===========================================================================
class BossBullet : public Character
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! コンストラクタ
	BossBullet(void);
	//! デストラクタ
	~BossBullet(void);

	//! 初期化
	void	init(void);
	//! 解放
	void	cleanUp(void);
	//!	弾の当たり用ステータス設定
	void	setState(D3DXVECTOR3 pos, D3DXVECTOR3 vec, u32 attribute, u32 hitAttribute);
	//!@}

	//! 更新
	void	upDate(void);
	//!	描画
	void	render(void);

private:
	SystemCollision::Object		_object2;	//!< 当たり判定用オブジェクト

	D3DXVECTOR3	_oldPos;	//!< 前回の位置
	D3DXVECTOR3	_vec;		//!< 移動する向き
	f32			_speed;		//!< 移動速度
	f32			_size;		//!< 弾のサイズ
};