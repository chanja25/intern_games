//===========================================================================
//!
//!	@file		Enemy3.cpp
//!	@brief		Enemy3クラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Enemy3::Enemy3(void)
{
}

//---------------------------------------------------------------------------
//	引数付きコンストラクタ
//!	@param pos	[in] 初期位置
//!	@param life	[in] 体力
//---------------------------------------------------------------------------
Enemy3::Enemy3(D3DXVECTOR3 pos, s32 life)
{
	_pos = pos;
	_life = life;
}

//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Enemy3::~Enemy3(void)
{
}

//---------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------
void	Enemy3::init(void)
{
	_pTexture = RESOURCE->loadTexture("Enemy3.bmp");

	// 敵の初期設定
	_moveCount	 = 45;
	_countSpeed = 1;//rand()%3+3;

	// ノードの設定
	_node.setObject(this);

	// 当たり判定用オブジェクトの初期化
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_ENEMY );

	// タスクの設定
	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy3::upDate(void)
{
	_alpha = 1.0f;

	// 弾に当たればライフを減らす
	if( _object.getState() ) {
		_life--;
		_alpha = 0.1f;
	}

	//	移動
	//_pos.z += cosf(_moveCount*(D3DX_PI/180)*2) * 0.1f;
	_pos.x += sinf(_moveCount*(D3DX_PI/180)*2) * 0.2f;

	_moveCount += _countSpeed;

	// ライフが無くなれば削除要請、あれば当たり判定をつける
	if( _life <= 0 ) {
		//	一定の確率でアイテムを落とす
		if( rand()%10 == 0 ) {
			Item* pItem = new Item();
			pItem->setState(_pos, D3DXVECTOR3(0, 0, -1), SystemCollision::Object::ATT_ITEM, SystemCollision::Object::ATT_PLAYER);
			TASK->addBottom(pItem, SystemTask::LIST_BULLET);
		}
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 0.8f);
		COLLISION->setObject(&_object);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Enemy3::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy3::render(void)
{
	// 球の描画
	PRIMITIVE->drawSphere(_pos, 0.8f, _pTexture, 4, _alpha);
}




