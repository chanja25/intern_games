//===========================================================================
//!
//!	@file		SystemPrimitive.cpp
//!	@brief		SystemPrimitiveクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	SystemPrimitive
//---------------------------------------------------------------------------
SystemPrimitive	SystemPrimitive::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemPrimitive::SystemPrimitive(void) :
_pVertex(NULL)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SystemPrimitive::~SystemPrimitive(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemPrimitive::init(void)
{
	_pVertex	= new LINEVERTEX[MAX_VERTEXBUFFER];
	_pTriangle	= new CUSTOMVERTEX[MAX_TRIANGLEBUFFER];

	ZeroMemory(_pVertex, sizeof(LINEVERTEX)*MAX_VERTEXBUFFER);
	ZeroMemory(_pTriangle, sizeof(CUSTOMVERTEX)*MAX_TRIANGLEBUFFER);

	_vertexNum		= 0;
	_triangleNum	= 0;
	_triangleNo		= 0;
	
	// エフェクトファイルの読み込み
	_shader.init("Shader.fx");
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemPrimitive::cleanUp(void)
{
	// Direct3Dデバイスの解放
	_shader.cleanUp();

	SAFE_DELETES(_pVertex);
	SAFE_DELETES(_pTriangle);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	SystemPrimitive::render(void)
{
	renderLine();

	// 描画要請を0にする
	_triangleNo = 0;
	_triangleNum = 0;
}

//---------------------------------------------------------------------------
//	ラインの描画
//---------------------------------------------------------------------------
void	SystemPrimitive::renderLine(void)
{
	// 描画するものが無ければ終わる
	if( _vertexNum <= 0 ) return;

	// Direct3DDeviceの取得
	LPDIRECT3DDEVICE9 pDevice = DIRECTX->getD3DDevice();

	// ローカル、ワールド座標の変換
	D3DXMATRIX world;
	D3DXMatrixIdentity(&world);

	// シェーダーの設定、開始
	_shader.setPass(Shader::PASS_LINE);
	_shader.setTexture(NULL);
	_shader.setMatrix(world);
	_shader.beginShader();

	// 描画する頂点バッファの設定
	pDevice->SetFVF(D3DFVF_LINEVERTEX);

	// ポリゴンの描画
	pDevice->DrawPrimitiveUP(D3DPT_LINELIST, _vertexNum/2, _pVertex, sizeof(LINEVERTEX));

	// シェーダーの終了
	_shader.endShader();

	// 描画要請を0にする
	_vertexNum = 0;
}

//---------------------------------------------------------------------------
//	テクスチャーの描画
//! @param	pos		[in] 表示位置
//!	@param pVB		[in] 表示するポリゴンの頂点情報
//!	@param pTexture	[in] 表示するテクスチャー
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	SystemPrimitive::drawTexture(const D3DXVECTOR3& pos, const D3DXMATRIX* mat, const LPDIRECT3DVERTEXBUFFER9 pVB, LPDIRECT3DTEXTURE9 pTexture, f32 alpha)
{
	// ローカル、ワールド座標の変換
	D3DXMATRIX world, trans, rotY;
	D3DXMatrixTranslation(&trans, pos.x, pos.y, pos.z);
	D3DXMatrixRotationY(&rotY, D3DX_PI);
	rotY._13 = 0;
	rotY._31 = 0;
	world = ( mat != NULL )? rotY * (*mat) * trans: rotY * trans;

	LPDIRECT3DDEVICE9	pDevice = DIRECTX->getD3DDevice();

	// シェーダーの設定、開始
	_shader.setPass(Shader::PASS_3D_POINTLIGHT);
	_shader.setAlpha(alpha);
	_shader.setTexture(pTexture);
	_shader.setMatrix(world);
	_shader.beginShader();

	// 描画する頂点バッファの設定
	pDevice->SetStreamSource(0, pVB, 0, sizeof(CUSTOMVERTEX));
	pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);

	// ポリゴンの描画
	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

	// シェーダーの終了
	_shader.endShader();

	return S_OK;
}

//---------------------------------------------------------------------------
//	テクスチャーの描画
//!	@param x		[in] x座標
//!	@param y		[in] y座標
//!	@param width	[in] 横幅
//!	@param height	[in] 縦幅
//!	@param pTexture	[in] 表示するテクスチャー
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	SystemPrimitive::drawTexture(f32 x, f32 y, f32 width, f32 height, const LPDIRECT3DTEXTURE9 pTexture, DWORD color)
{
	// 頂点情報の設定
	CUSTOMVERTEX2D vertices[] = {
		{ x,			y,			0.5f, 1.0f, color, 0.0f, 0.0f },
		{ x + width,	y,			0.5f, 1.0f, color, 1.0f, 0.0f },
		{ x,			y + height,	0.5f, 1.0f, color, 0.0f, 1.0f },
		{ x + width,	y + height,	0.5f, 1.0f, color, 1.0f, 1.0f },
	};

	// シェーダーの設定、開始
	_shader.setPass(Shader::PASS_2D);
	_shader.setTexture(pTexture);
	_shader.beginShader();

	LPDIRECT3DDEVICE9 pDevice = DIRECTX->getD3DDevice();

	// 描画する頂点バッファの設定
	pDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1);

	// ポリゴンの描画
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(CUSTOMVERTEX2D));

	// シェーダーの終了
	_shader.endShader();

	return S_OK;
}
//---------------------------------------------------------------------------
//	テクスチャーの描画
//!	@param x		[in] x座標
//!	@param y		[in] y座標
//!	@param width	[in] 横幅
//!	@param height	[in] 縦幅
//!	@param color	[in] 表示する色
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	SystemPrimitive::drawRect(f32 x, f32 y, f32 width, f32 height, DWORD color)
{
	// 頂点情報の設定
	CUSTOMVERTEX2D vertices[] = {
		{ x,			y,			0.5f, 1.0f, color, 0.0f, 0.0f },
		{ x + width,	y,			0.5f, 1.0f, color, 1.0f, 0.0f },
		{ x,			y + height,	0.5f, 1.0f, color, 0.0f, 1.0f },
		{ x + width,	y + height,	0.5f, 1.0f, color, 1.0f, 1.0f },
	};

	// シェーダーの設定、開始
	_shader.setPass(Shader::PASS_2D_RECT);
	_shader.beginShader();

	LPDIRECT3DDEVICE9 pDevice = DIRECTX->getD3DDevice();

	// 描画する頂点バッファの設定
	pDevice->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);

	// ポリゴンの描画
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vertices, sizeof(CUSTOMVERTEX2D));

	// シェーダーの終了
	_shader.endShader();

	return S_OK;
}

//---------------------------------------------------------------------------
//	ポリゴンの描画
//!	@param	pos			[in] 位置
//!	@param	pTexture	[in] 貼り付けるテクスチャー
//! @retval	S_OK	成功
//! @retval	E_FAIL	失敗
//---------------------------------------------------------------------------
HRESULT	SystemPrimitive::drawTriangle(const D3DXVECTOR3& pos, LPDIRECT3DTEXTURE9 pTexture, f32 alpha)
{
	// 描画するものが無ければ終わる
	if( _triangleNum - _triangleNo <= 0 ) return E_FAIL;


	// ローカル、ワールド座標の変換
	D3DXMATRIX world, trans;//, rotY;
	D3DXMatrixTranslation(&trans, pos.x, pos.y, pos.z);
	//D3DXMatrixRotationY(&rotY, 0);
	world = /*rotY */ trans;

	LPDIRECT3DDEVICE9	pDevice = DIRECTX->getD3DDevice();

	// シェーダーの設定、開始
	_shader.setPass(Shader::PASS_3D_POINTLIGHT);
	_shader.setAlpha(alpha);
	_shader.setTexture(pTexture);
	_shader.setMatrix(world);
	_shader.beginShader();

	// 描画する頂点バッファの設定
	pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);

	//BEGIN_PROFILE("test01", Profiler::COLOR_YELLOW);

	u32 num = (_triangleNum - _triangleNo + 1) / 3;

	// ポリゴンの描画
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST , num, &_pTriangle[_triangleNo], sizeof(CUSTOMVERTEX));

	//END_PROFILE("test01");

	// シェーダーの終了
	_shader.endShader();

	_triangleNo = _triangleNum;

	return S_OK;
}

//---------------------------------------------------------------------------
//	頂点情報を追加
//!	@param	pVertex		[in] セットする頂点情報の先頭アドレス
//!	@param	vertexNum	[in] セットするラインの数
//---------------------------------------------------------------------------
void	SystemPrimitive::setVertex(LINEVERTEX *pVertex, u32 vertexNum)
{
	// 頂点のセットできる領域外なら終わる
	if( _vertexNum+vertexNum >= MAX_VERTEXBUFFER ) return;

	// 頂点情報を入れる
	for( u32 i=0; i<vertexNum; i++ ) {
		_pVertex[_vertexNum] = pVertex[i];
		_vertexNum++;
	}
}

//---------------------------------------------------------------------------
//	頂点情報を追加
//!	@param	pVertex		[in] セットする頂点情報の先頭アドレス
//!	@param	vertexNum	[in] セットするラインの数
//---------------------------------------------------------------------------
void	SystemPrimitive::setTriangle(CUSTOMVERTEX *pVertex)
{
	// 頂点のセットできる領域外なら終わる
	if( _triangleNum+3 >= MAX_TRIANGLEBUFFER ) return;

	// 頂点情報を入れる
	for( u32 i=0; i<3; i++ ) {
		_pTriangle[_triangleNum] = pVertex[i];
		_triangleNum++;
	}
}

//---------------------------------------------------------------------------
//	スフィアの描画要請
//!	@param pos		[in] 表示位置
//!	@param vec		[in] 向き
//!	@param color	[in] 描画色
//---------------------------------------------------------------------------
void	SystemPrimitive::drawArrow(const D3DXVECTOR3& pos, const D3DXVECTOR3& vec, DWORD color)
{
	if( _vertexNum+6 >= MAX_VERTEXBUFFER ) return;

	D3DXVECTOR3 cross, vec1, vec2;
	vec2 = D3DXVECTOR3(0, 1, 0);

	cross.x = vec.y * vec2.z - vec2.y * vec.z;
	cross.y = vec.z * vec2.x - vec2.z * vec.x;
	cross.z = vec.x * vec2.y - vec2.x * vec.y;
	D3DXVec3Normalize(&cross, &cross);
	D3DXVec3Normalize(&vec1, &vec);

	f32 angle = (vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z) / (D3DXVec3Length(&vec1) * D3DXVec3Length(&vec2));

	angle = acosf(angle);
	D3DXMATRIX	trans;

	DIRECTX->axisRotationMatrix(&trans, cross, angle);

	LINEVERTEX vertex[6];

	for( u32 i=0; i<3; i++ ) {
		vec1.x	= 0;
		vec1.y	= 0;
		vec1.z	= 0;
		vertex[i*2].color = color;
		D3DXVec3TransformNormal(&vec1, &vec1, &trans);
		vertex[i*2].x = pos.x + vec1.x;
		vertex[i*2].y =	pos.y + vec1.y;
		vertex[i*2].z =	pos.z + vec1.z;
	}

	vec1.x	= 0;
	vec1.y	= -2.0f;
	vec1.z	= 0;
	vertex[1].color = color;
	D3DXVec3TransformNormal(&vec1, &vec1, &trans);
	vertex[1].x = pos.x + vec1.x;
	vertex[1].y = pos.y + vec1.y;
	vertex[1].z = pos.z + vec1.z;

	vec1.x	= -0.5f;
	vec1.y	= -0.5f;
	vec1.z	= 0;
	vertex[3].color = color;
	D3DXVec3TransformNormal(&vec1, &vec1, &trans);
	vertex[3].x = pos.x + vec1.x;
	vertex[3].y = pos.y + vec1.y;
	vertex[3].z = pos.z + vec1.z;

	vec1.x	=  0.5f;
	vec1.y	= -0.5f;
	vec1.z	= 0;
	vertex[5].color = color;
	D3DXVec3TransformNormal(&vec1, &vec1, &trans);
	vertex[5].x = pos.x + vec1.x;
	vertex[5].y = pos.y + vec1.y;
	vertex[5].z = pos.z + vec1.z;

	//	頂点情報のセット
	setVertex(vertex, 6);
}

//---------------------------------------------------------------------------
//	スフィアの描画要請
//!	@param pos			[in] 表示位置
//!	@param radius		[in] 半径
//!	@param color		[in] 描画色
//!	@param divideCount	[in] 表示精度
//---------------------------------------------------------------------------
void	SystemPrimitive::drawSphere(const D3DXVECTOR3& pos, f32 radius, DWORD color, u32 divideCount)
{
	//	プリミティブの表示数の制限	
	divideCount = ( divideCount > 64 )? 64: ( divideCount < 4 )? 4: divideCount;

	//	プリミティブが表示可能数を超えていれば終わる
	if( _vertexNum+divideCount*6 >= MAX_VERTEXBUFFER ) return;


	//	一つのプリミティブ当りの角度
	f32 angle = D3DX_PI/180 * (360.0f / static_cast<f32>(divideCount));

	//	スフィアの頂点の設定
	LINEVERTEX vertex[512];

	//	XY軸上のスフィア設定
	u32 i;
	for( i=1; i<divideCount*2; i++ ) {
		vertex[i-1].x = pos.x - cos((i/2) * angle) * radius;
		vertex[i-1].y = pos.y - sin((i/2) * angle) * radius;
		vertex[i-1].z = pos.z;
		vertex[i-1].color = color;
	}

	vertex[i-1].x = pos.x - cos((i/2) * angle) * radius;
	vertex[i-1].y = pos.y - sin((i/2) * angle) * radius;
	vertex[i-1].z = pos.z;
	vertex[i-1].color = color;

	//	YZ軸上のスフィア設定
	for( i++; i<divideCount*4; i++ ) {
		vertex[i-1].x = pos.x;
		vertex[i-1].y = pos.y - sin((i/2) * angle) * radius;
		vertex[i-1].z = pos.z - cos((i/2) * angle) * radius;
		vertex[i-1].color = color;
	}

	vertex[i-1].x = pos.x;
	vertex[i-1].y = pos.y - sin((i/2) * angle) * radius;
	vertex[i-1].z = pos.z - cos((i/2) * angle) * radius;
	vertex[i-1].color = color;

	//	ZX軸上のスフィア設定
	for( i++; i<divideCount*6; i++ ) {
		vertex[i-1].x = pos.x - sin((i/2) * angle) * radius;
		vertex[i-1].y = pos.y;
		vertex[i-1].z = pos.z - cos((i/2) * angle) * radius;
		vertex[i-1].color = color;
	}

	vertex[i-1].x = pos.x - sin((i/2) * angle) * radius;
	vertex[i-1].y = pos.y;
	vertex[i-1].z = pos.z - cos((i/2) * angle) * radius;
	vertex[i-1].color = color;

	//	頂点情報のセット
	setVertex(vertex, divideCount*6);
}

//---------------------------------------------------------------------------
//	カプセルの描画要請
//!	@param pos			[in] 表示位置
//!	@param width		[in] 幅
//!	@param radius		[in] 半径
//!	@param color		[in] 描画色
//!	@param divideCount	[in] 表示精度
//---------------------------------------------------------------------------
void	SystemPrimitive::drawCapsule(const D3DXVECTOR3& pos1, const D3DXVECTOR3& pos2, f32 radius, DWORD color, u32 divideCount)
{
	//	プリミティブの表示数の制限	
	divideCount = ( divideCount > 64 )? 64: ( divideCount < 4 )? 4: divideCount;

	//	2点が同じ位置ならスフィアを作る
	if( pos1 == pos2 ) {
		drawSphere(pos1, radius, color, divideCount);
		return;
	}

	//	プリミティブが表示可能数を超えていれば終わる
	if( _vertexNum+divideCount*8+8 >= MAX_VERTEXBUFFER ) return;

	D3DXVECTOR3 cross, vec1, vec2;
	vec1 = pos2 - pos1;
	vec2 = D3DXVECTOR3(0, 0, 1);

	cross.x = vec1.y * vec2.z - vec2.y * vec1.z;
	cross.y = vec1.z * vec2.x - vec2.z * vec1.x;
	cross.z = vec1.x * vec2.y - vec2.x * vec1.y;
	D3DXVec3Normalize(&cross, &cross);
	D3DXVec3Normalize(&vec1, &vec1);

	f32 angle = (vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z) / (D3DXVec3Length(&vec1) * D3DXVec3Length(&vec2));

	angle = acosf(angle);
	D3DXMATRIX	trans;

	DIRECTX->axisRotationMatrix(&trans, cross, angle);

	D3DXVECTOR3 vec;

	//	一つ当りのプリミティブ角度
	angle = D3DX_PI/180 * (360.0f / static_cast<f32>(divideCount));

	//	スフィアの頂点の設定
	LINEVERTEX vertex[520];

	//	１つ目のXY軸上のスフィア設定
	u32 i;
	for( i=1; i<divideCount*2; i++ ) {
		vec.x = cos((i/2) * angle) * radius;
		vec.y = sin((i/2) * angle) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos1.x - vec.x;
		vertex[i-1].y = pos1.y - vec.y;
		vertex[i-1].z = pos1.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = cos((i/2) * angle) * radius;
	vec.y = sin((i/2) * angle) * radius;
	vec.z = 0;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos1.x - vec.x;
	vertex[i-1].y = pos1.y - vec.y;
	vertex[i-1].z = pos1.z - vec.z;
	vertex[i-1].color = color;

	//	２つ目のXY軸上のスフィア設定
	for( i++; i<divideCount*4; i++ ) {
		vec.x = cos((i/2) * angle) * radius;
		vec.y = sin((i/2) * angle) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos2.x - vec.x;
		vertex[i-1].y = pos2.y - vec.y;
		vertex[i-1].z = pos2.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = cos((i/2) * angle) * radius;
	vec.y = sin((i/2) * angle) * radius;
	vec.z = 0;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos2.x - vec.x;
	vertex[i-1].y = pos2.y - vec.y;
	vertex[i-1].z = pos2.z - vec.z;
	vertex[i-1].color = color;

	//	1つ目のYZ軸上のスフィア設定
	for( i++; i<divideCount*5; i++ ) {
		vec.x = 0;
		vec.y = sin((i/2) * angle - (D3DX_PI/2)) * radius;
		vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos1.x - vec.x;
		vertex[i-1].y = pos1.y - vec.y;
		vertex[i-1].z = pos1.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = 0;
	vec.y = sin((i/2) * angle - (D3DX_PI/2)) * radius;
	vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos1.x - vec.x;
	vertex[i-1].y = pos1.y - vec.y;
	vertex[i-1].z = pos1.z - vec.z;
	vertex[i-1].color = color;

	//	2つ目のYZ軸上のスフィア設定
	for( i++; i<divideCount*6; i++ ) {
		vec.x = 0;
		vec.y = sin((i/2) * angle - (D3DX_PI/2)) * radius;
		vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos2.x - vec.x;
		vertex[i-1].y = pos2.y - vec.y;
		vertex[i-1].z = pos2.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = 0;
	vec.y = sin((i/2) * angle - (D3DX_PI/2)) * radius;
	vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos2.x - vec.x;
	vertex[i-1].y = pos2.y - vec.y;
	vertex[i-1].z = pos2.z - vec.z;
	vertex[i-1].color = color;

	//	1つ目のZX軸上のスフィア設定
	for( i++; i<divideCount*7; i++ ) {
		vec.x = sin((i/2) * angle - (D3DX_PI/2)) * radius;
		vec.y = 0;
		vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos1.x - vec.x;
		vertex[i-1].y = pos1.y - vec.y;
		vertex[i-1].z = pos1.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = sin((i/2) * angle - (D3DX_PI/2)) * radius;
	vec.y = 0;
	vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos1.x - vec.x;
	vertex[i-1].y = pos1.y - vec.y;
	vertex[i-1].z = pos1.z - vec.z;
	vertex[i-1].color = color;

	//	2つ目のZX軸上のスフィア設定
	for( i++; i<divideCount*8; i++ ) {
		vec.x = sin((i/2) * angle - (D3DX_PI/2)) * radius;
		vec.y = 0;
		vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos2.x - vec.x;
		vertex[i-1].y = pos2.y - vec.y;
		vertex[i-1].z = pos2.z - vec.z;
		vertex[i-1].color = color;
	}

	vec.x = sin((i/2) * angle - (D3DX_PI/2)) * radius;
	vec.y = 0;
	vec.z = cos((i/2) * angle - (D3DX_PI/2)) * radius;
	D3DXVec3TransformNormal(&vec, &vec, &trans);
	vertex[i-1].x = pos2.x - vec.x;
	vertex[i-1].y = pos2.y - vec.y;
	vertex[i-1].z = pos2.z - vec.z;
	vertex[i-1].color = color;

	//	前後をつなぐライン設定
	for( i++; i<divideCount*8+8; i+=2 ) {
		vec.x = cos((i/2) * D3DX_PI/2) * radius;
		vec.y = sin((i/2) * D3DX_PI/2) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i-1].x = pos1.x - vec.x;
		vertex[i-1].y = pos1.y - vec.y;
		vertex[i-1].z = pos1.z - vec.z;
		vertex[i-1].color = color;

		vec.x = cos((i/2) * D3DX_PI/2) * radius;
		vec.y = sin((i/2) * D3DX_PI/2) * radius;
		vec.z = 0;
		D3DXVec3TransformNormal(&vec, &vec, &trans);
		vertex[i].x = pos2.x - vec.x;
		vertex[i].y = pos2.y - vec.y;
		vertex[i].z = pos2.z - vec.z;
		vertex[i].color = color;
	}

	//	頂点情報のセット
	setVertex(vertex, divideCount*8+8);
}

//---------------------------------------------------------------------------
//	スフィアの描画
//!	@param pos			[in] 表示位置
//!	@param radius		[in] 半径
//!	@param color		[in] 描画色
//!	@param divideCount	[in] 表示精度
//---------------------------------------------------------------------------
void	SystemPrimitive::drawSphere(const D3DXVECTOR3& pos, f32 radius, LPDIRECT3DTEXTURE9 pTexture, u32 divideCount, f32 alpha)
{
	//	プリミティブの表示数の制限	
	divideCount = ( divideCount > 30 )? 30: ( divideCount < 4 )? 4: divideCount;

	//	一つのプリミティブ当りの角度
	f32 angle = D3DX_PI/180 * (360.0f / divideCount);

	f32 distu = 1.0f / (divideCount);
	f32 distv = 1.0f / (divideCount/2);
	f32 tu = 0;
	f32 tv = 0;

	//	スフィアの頂点の設定
	static CUSTOMVERTEX vertex[3];

	u32 i, j;
	//	スフィアの上部
	for( i=0; i<divideCount; i++ ) {
		vertex[0].x  = 0;
		vertex[0].y  = radius;
		vertex[0].z  = 0;
		vertex[0].tu = tu;
		vertex[0].tv = 0;
		vertex[0].color = 0xffffffff;
		vertex[0].nx = vertex[0].x;
		vertex[0].ny = vertex[0].y;
		vertex[0].nz = vertex[0].z;

		vertex[1].x  = sin(i*angle) * radius * sin(angle);
		vertex[1].y  = cos(angle) * radius;
		vertex[1].z  = cos(i*angle) * radius * sin(angle);
		vertex[1].tu = tu;
		vertex[1].tv = tv + distv;
		vertex[1].color = 0xffffffff;
		vertex[1].nx = vertex[1].x;
		vertex[1].ny = vertex[1].y;
		vertex[1].nz = vertex[1].z;

		vertex[2].x  = sin((i+1)*angle) * radius * sin(angle);
		vertex[2].y  = cos(angle) * radius;
		vertex[2].z  = cos((i+1)*angle) * radius * sin(angle);
		vertex[2].tu = tu - distu;
		vertex[2].tv = tv + distv;
		vertex[2].color = 0xffffffff;
		vertex[2].nx = vertex[2].x;
		vertex[2].ny = vertex[2].y;
		vertex[2].nz = vertex[2].z;

		setTriangle(vertex);
		tu -= distu;
	}
	tu = 0;
	tv += distv;

	//	スフィアの中部
	for( i=1; i<divideCount/2-1; i++ ) {
		for( j=0; j<divideCount; j++ ) {
			vertex[0].x  = sin(j*angle) * radius * sin(i*angle);
			vertex[0].y  = cos(i*angle) * radius;
			vertex[0].z  = cos(j*angle) * radius * sin(i*angle);
			vertex[0].tu = tu;
			vertex[0].tv = tv;
			vertex[0].color = 0xffffffff;
			vertex[0].nx = vertex[0].x;
			vertex[0].ny = vertex[0].y;
			vertex[0].nz = vertex[0].z;

			vertex[1].x  = sin(j*angle) * radius * sin((i+1)*angle);
			vertex[1].y  = cos((i+1)*angle) * radius;
			vertex[1].z  = cos(j*angle) * radius * sin((i+1)*angle);
			vertex[1].tu = tu;
			vertex[1].tv = tv + distv;
			vertex[1].color = 0xffffffff;
			vertex[1].nx = vertex[1].x;
			vertex[1].ny = vertex[1].y;
			vertex[1].nz = vertex[1].z;

			vertex[2].x  = sin((j+1)*angle) * radius * sin(i*angle);
			vertex[2].y  = cos(i*angle) * radius;
			vertex[2].z  = cos((j+1)*angle) * radius * sin(i*angle);
			vertex[2].tu = tu - distu;
			vertex[2].tv = tv;
			vertex[2].color = 0xffffffff;
			vertex[2].nx = vertex[2].x;
			vertex[2].ny = vertex[2].y;
			vertex[2].nz = vertex[2].z;

			setTriangle(vertex);

			vertex[0].x  = sin((j+1)*angle) * radius * sin(i*angle);
			vertex[0].y  = cos(i*angle) * radius;
			vertex[0].z  = cos((j+1)*angle) * radius * sin(i*angle);
			vertex[0].tu = tu - distu;
			vertex[0].tv = tv;
			vertex[0].color = 0xffffffff;
			vertex[0].nx = vertex[0].x;
			vertex[0].ny = vertex[0].y;
			vertex[0].nz = vertex[0].z;

			vertex[1].x  = sin(j*angle) * radius * sin((i+1)*angle);
			vertex[1].y  = cos((i+1)*angle) * radius;
			vertex[1].z  = cos(j*angle) * radius * sin((i+1)*angle);
			vertex[1].tu = tu;
			vertex[1].tv = tv + distv;
			vertex[1].color = 0xffffffff;
			vertex[1].nx = vertex[1].x;
			vertex[1].ny = vertex[1].y;
			vertex[1].nz = vertex[1].z;

			vertex[2].x  = sin((j+1)*angle) * radius * sin((i+1)*angle);
			vertex[2].y  = cos((i+1)*angle) * radius;
			vertex[2].z  = cos((j+1)*angle) * radius * sin((i+1)*angle);
			vertex[2].tu = tu - distu;
			vertex[2].tv = tv + distv;
			vertex[2].color = 0xffffffff;
			vertex[2].nx = vertex[2].x;
			vertex[2].ny = vertex[2].y;
			vertex[2].nz = vertex[2].z;
			
			setTriangle(vertex);
			tu -= distu;
		}
		tu = 0;
		tv += distv;
	}

	//	スフィアの下部
	for( i=0; i<divideCount; i++ ) {
		vertex[0].x  = sin(i*angle) * radius * sin((divideCount/2-1)*angle);
		vertex[0].y  = cos((divideCount/2-1)*angle) * radius;
		vertex[0].z  = cos(i*angle) * radius * sin((divideCount/2-1)*angle);
		vertex[0].tu = tu;
		vertex[0].tv = tv;
		vertex[0].color = 0xffffffff;
		vertex[0].nx = vertex[0].x;
		vertex[0].ny = vertex[0].y;
		vertex[0].nz = vertex[0].z;

		vertex[1].x  = 0;
		vertex[1].y  = -radius;
		vertex[1].z  = 0;
		vertex[1].tu = tu;
		vertex[1].tv = tv + distv;
		vertex[1].color = 0xffffffff;
		vertex[1].nx = vertex[1].x;
		vertex[1].ny = vertex[1].y;
		vertex[1].nz = vertex[1].z;

		vertex[2].x  = sin((i+1)*angle) * radius * sin((divideCount/2-1)*angle);
		vertex[2].y  = cos((divideCount/2-1)*angle) * radius;
		vertex[2].z  = cos((i+1)*angle) * radius * sin((divideCount/2-1)*angle);
		vertex[2].tu = tu - distu;
		vertex[2].tv = tv;
		vertex[2].color = 0xffffffff;
		vertex[2].nx = vertex[2].x;
		vertex[2].ny = vertex[2].y;
		vertex[2].nz = vertex[2].z;

		setTriangle(vertex);
		tu -= distu;
	}

	//	描画
	drawTriangle(pos, pTexture, alpha);
}



//---------------------------------------------------------------------------
//	インスタンスの取得
//!	@retval	インスタンスの取得
//---------------------------------------------------------------------------
SystemPrimitive*	SystemPrimitive::getInstance(void)
{
	return &_instance;
}