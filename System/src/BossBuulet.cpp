//===========================================================================
//!
//!	@file		BossBullet.cpp
//!	@brief		BossBulletクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
BossBullet::BossBullet(void)
{
}


//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
BossBullet::~BossBullet(void)
{
}


//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	BossBullet::init(void)
{
	//	テクスチャーの読み込み
	_pTexture = RESOURCE->loadTexture("Bullet2.bmp");

	// 弾の初期設定
	_oldPos = _pos;

	_life = 10;
	_size = 5.0f;

	_node.setObject(this);

	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	BossBullet::upDate(void)
{
	//	弾の移動
	_oldPos	= _pos;
	_pos += _vec * _speed;

	//	プレイヤーの弾と当たっていればライフが減り小さくなる
	u32 state = _object2.getState();
	if( state ) {
		if( state == SystemCollision::Object::ATT_PLAYER_BULLET ) {
			_life--;
			_size -= 0.35f;
		}
		_object2.setState(0);
	}
	//	プレイヤーに当たっているか、範囲外に出ると弾は消える
	if( _object.getState() || (_pos.z > 80) || (_pos.z < -10) ) {
		_state = STATE_INVALID;
	} else {
		//	弾が消えていなければプレイヤーとの当たり判定をセット
		_object.setObject(_pos, _size, _oldPos);
		COLLISION->setObject(&_object);

	}

	//	ライフが0の場合、向きと攻撃属性を変える
	if( _life == 0 ) {
		_vec = -_vec;
		_speed = 0.5f;
		_life = -1;
		_object.init(SystemCollision::Object::SHAPE_CAPSULE, SystemCollision::Object::ATT_BOSS_BULLET, SystemCollision::Object::ATT_BOSS);
		RESOURCE->releaseTexture(_pTexture);
		_pTexture = RESOURCE->loadTexture("Bullet3.bmp");
	} else {
		//	プレイヤーの弾との当たり判定をセット
		_object2.setObject(_pos, _size);
		COLLISION->setObject(&_object2);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	BossBullet::cleanUp(void)
{
	//	テクスチャーの解放
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	BossBullet::render(void)
{
	// 弾の描画
	PRIMITIVE->drawSphere(_pos, _size, _pTexture, 16);
}

//---------------------------------------------------------------------------
//	弾のステータス設定
//!	@param	pos			 [in]	弾の発射位置
//!	@param	attribute	 [in]	弾の属性
//!	@param	hitAttribute [in]	弾の当たり属性
//---------------------------------------------------------------------------
void	BossBullet::setState(D3DXVECTOR3 pos, D3DXVECTOR3 vec, u32 attribute, u32 hitAttribute)
{
	_pos = pos;
	D3DXVec3Normalize(&_vec, &vec);
	_speed = 0.3f;

	_object.init(SystemCollision::Object::SHAPE_CAPSULE, attribute, hitAttribute);
	_object2.init(SystemCollision::Object::SHAPE_SPHERE, SystemCollision::Object::ATT_BOSS_BULLET);
}
