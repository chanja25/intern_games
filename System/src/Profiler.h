//===========================================================================
//!
//!	@file		Profiler.h
//!	@brief		プロファイラ
//!
//===========================================================================
#ifndef PROFILER_H
#define	PROFILER_H

#pragma once

//#if _DEBUG
//	#define	USE_PROFILER	1
//#else //~#if _DEBUG
//	#define	USE_PROFILER	0
//#endif //~#if _DEBUG
#define	USE_PROFILER	1

#if USE_PROFILER

//===========================================================================
// タイマー
//===========================================================================
class Timer
{
public:
		//! デフォルトコンストラクタ
		Timer();

		//! 経過時間の取得
		u32	get(void);

		//! 経過時間のリセット
		void reset(void);

		// 処理計測単位
		static const u32 TICK_DELTA = 1000000000;		//!< ナノ秒の逆数

private:
		u64	_baseFreq;
		u64	_baseTime;
};


//===========================================================================
// プロファイラ
//===========================================================================
class Profiler {

public:
		//! ログ表示色
		enum COLOR {
			COLOR_WHITE,
			COLOR_RED,
			COLOR_GREEN,
			COLOR_BLUE,
			COLOR_YELLOW,
			COLOR_BLACK,
		};

		//! プロファイルログ
		struct Log {
			Log*		_pParent;		//!< 親ログのポインタ
			Log*		_pNext;			//!< 同列(兄弟)ログのポインタ
			Log*		_pChild;		//!< 子ログのポインタ

			// 計測用パラメータ
			u32			_beginTime;		//!< 計測開始時間
			u32			_endTime;		//!< 計測終了時間
			u32			_tmpCount;		//!< 計測回数
			u32			_tmpTotalTime;	//!< 合計処理時間
			u32			_tmpMinTime;	//!< 最小処理時間
			u32			_tmpMaxTime;	//!< 最大処理時間

			// 表示用パラメータ
			const char*	_pName;			//!< プロファイル名
			COLOR		_color;			//!< 表示色
			u32			_count;			//!< 計測回数
			u32			_totalTime;		//!< 合計処理時間
			u32			_averageTime;	//!< 平均処理時間
			u32			_minTime;		//!< 最小処理時間
			u32			_maxTime;		//!< 最大処理時間
		};

		//! 初期化
		static bool init(void);

		//! 終了
		static void cleanup(void);

		//! インスタンスの取得
		static Profiler* getInstancePtr(void);

		//! フレームの開始をプロファイラに伝える
		void startFrame(void);

		//! 計測開始
		void begin(const char* pName, COLOR color);

		//! 計測終了
		void end(const char* pName);

		//! ルートログの取得
		Log* getRootLog(void);

private:

		//! デフォルトコンストラクタ
		Profiler();

		//! ログ情報の生成
		Log* createLog(const char* pName, Log* pParent);

		//! ログ情報の更新
		void updateLog(Log* pLog);

		//!  唯一のインスタンス
		static Profiler* _pInstance;

		//! タイマー
		Timer _timer;

		static const u32 LOG_COUNT;
		Log* _pRootLog;
		Log* _pCurrentLog;
		u32 _logCount;

};

//===========================================================================
// スコープ内処理時間計測プロファイラ
//===========================================================================
class ScopeProfiler
{
public:
		//! コンストラクタ
		ScopeProfiler(const char* pName, Profiler::COLOR color = Profiler::COLOR_GREEN);

		//! デストラクタ
		virtual ~ScopeProfiler(void);

private:
		//! プロファイル名
		const char*	_pName;
};

#define BEGIN_PROFILE(name, color)	Profiler::getInstancePtr()->begin(name, color)
#define END_PROFILE(name)			Profiler::getInstancePtr()->end(name)
#define PROFILE						ScopeProfiler scopeProfiler

#else //~#if USE_PROFILER

#define BEGIN_PROFILE(name, color)
#define END_PROFILE(name)
#define PROFILE(name, ...)

#endif //~#if USE_PROFILER

#endif //~#if PROFILER_H

//===========================================================================
//	END OF FILE
//===========================================================================
