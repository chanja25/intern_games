//===========================================================================
//!
//!	@file		SystemResource.cpp
//!	@brief		SystemResourceクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	インスタンスの実体化
//---------------------------------------------------------------------------
SystemResource	SystemResource::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemResource::SystemResource(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SystemResource::~SystemResource(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemResource::init(void)
{
	for(u32 i=0; i<MAX_INFORMATION; i++ ) {
		_info[i]._count		= 0;
		_info[i]._pTexture	= NULL;
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemResource::cleanUp(void)
{
	for(u32 i=0; i<MAX_INFORMATION; i++ ) {
		if( _info[i]._count ==0 ) continue;
		SAFE_RELEASE(_info[i]._pTexture);
	}
}

//---------------------------------------------------------------------------
//	テクスチャーの読み込み
//!	@param	pFileName [in] 読み込むファイル名
//!	@retval	読み込んだテクスチャー
//---------------------------------------------------------------------------
LPDIRECT3DTEXTURE9	SystemResource::loadTexture(LPSTR pFileName)
{
	u32 i;

	//	同じテクスチャーが既に読み込まれていないか確認
	for( i=0; i<MAX_INFORMATION; i++ ) {
		if( _info[i]._count == 0 ) continue;
		//	読み込まれていれば、使用数を一つ増やして、テクスチャーを返す
		if( strcmp(_info[i]._name, pFileName) == 0 ) {
			_info[i]._count++;
			return _info[i]._pTexture;
		}
	}

	//	現在使用していないリソースの管理場所を探す
	for( i=0; i<MAX_INFORMATION; i++ ) {
		if( _info[i]._count == 0 ) break;
	}

	//	テクスチャーが管理数を超えていないか確認
	if( i >= MAX_INFORMATION ) {
		ASSERT(false, "リソースの管理可能数を超えました。");
		return NULL;
	}

	//	テクスチャーの読み込み
	LPDIRECT3DTEXTURE9 pTexture = NULL;

	if( FAILED(D3DXCreateTextureFromFile(DIRECTX->getD3DDevice(), pFileName, &pTexture)) ) {
		ASSERT(false, "テクスチャーの読み込みに失敗しました。");
		return NULL;
	}

	//	リソース情報を入れてテクスチャーを返す
	strcpy(_info[i]._name, pFileName);
	_info[i]._count++;
	_info[i]._pTexture = pTexture;

	return pTexture;
}

//---------------------------------------------------------------------------
//	テクスチャーの解放
//!	@param	pTexture [in] 解放するテクスチャー
//---------------------------------------------------------------------------
void	SystemResource::releaseTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	//	テクスチャーの中身があるか確認
	if( !pTexture ) return;

	//	テクスチャーが管理されている場所を探す
	u32 i;
	for( i=0; i<MAX_INFORMATION; i++ ) {
		if( pTexture == _info[i]._pTexture ) break;
	}

	//	管理していないテクスチャーの場合解放して終わる
	if( i >= MAX_INFORMATION ) {
		SAFE_RELEASE(pTexture);
		return;
	}

	//	テクスチャーの使用数を減らし、0になっていれば解放する
	_info[i]._count--;
	if( _info[i]._count == 0 ) {
		SAFE_RELEASE(_info[i]._pTexture);
	}
}

//---------------------------------------------------------------------------
//	インスタンスの取得
//!	@retval	インスタンス
//---------------------------------------------------------------------------
SystemResource*	SystemResource::getInstance(void)
{
	return &_instance;
}

