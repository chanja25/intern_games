//===========================================================================
//!
//!	@file		Player2.cpp
//!	@brief		Player2クラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Player2::Player2(void)
{
}


//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Player2::~Player2(void)
{
}


//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Player2::init(void)
{
	_pTexture = RESOURCE->loadTexture("Player2.bmp");

	// プレイヤーの初期設定
	_pos.x = 0;
	_pos.y = 0;
	_pos.z = 1;

	_life = 10;


	_node.setObject(this);
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_PLAYER );

	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Player2::upDate(void)
{
	//---------------------------------------------------------------------------
	// ボタンの入力で移動する
	//---------------------------------------------------------------------------
	D3DXVECTOR2 thumb = IInput->getLeftThumb(1);

	_pos.z += .13f * thumb.y;
	if( _pos.z < 1.0f ) _pos.z = 1.0f;
	if( _pos.z > 9.0f ) _pos.z = 9.0f;
	_pos.x += .13f * thumb.x;
	if( _pos.x < -14.0f ) _pos.x = -14.0f;
	if( _pos.x >  14.0f ) _pos.x =  14.0f;

	//	Aボタンを押すと弾を発射する
	if( IInput->isButtonPush(0, XInput::BUTTON_A) ) {
		if( _bulletLevel == 0 ) {
			Bullet* pBullet = new Bullet();
			pBullet->setState(_pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		} else if( _bulletLevel == 1 ){
			D3DXVECTOR3 pos = _pos;
			pos.x -= 0.5f;
			Bullet* pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 1.0f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		} else {
			D3DXVECTOR3 pos = _pos;
			pos.x -= 0.5f;
			Bullet* pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(-0.2f, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 0.5f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
			pos.x += 0.5f;
			pBullet = new Bullet();
			pBullet->setState(pos, D3DXVECTOR3(0.2f, 0, 1), SystemCollision::Object::ATT_PLAYER_BULLET, SystemCollision::Object::ATT_ENEMY | SystemCollision::Object::ATT_BOSS_BULLET | SystemCollision::Object::ATT_BOSS);
			TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
		}
	}

	_alpha = 1.0f;

	// 攻撃を受けていればライフを減らす
	u32 state = _object.getState();
	if( state ) {
		if( state == SystemCollision::Object::ATT_ENEMY_BULLET ) {
			_life--;
			_alpha = 0.1f;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_ENEMY_BULLET2 ){
			_life -= 2;
			_alpha = 0.1f;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_BOSS_BULLET ){
			_life -= 5;
			_alpha = 0.1f;
			if( _bulletLevel > 0 )_bulletLevel--;
		}
		if( state == SystemCollision::Object::ATT_ITEM ){
			if( _bulletLevel < 2 ) {
				_bulletLevel++;
			} else {
				_life += 2;
				if( _life > 10 ) _life = 10;
			}
		}
	}

	// ライフが0なら削除要請を出す。そうでなければ当たり判定をセットする
	if( _life <= 0 ) {
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 1.0f);
		COLLISION->setObject(&_object);
	}

	// 二人プレイ用のカメラの位置にセットする
	CAMERA->setTarget(D3DXVECTOR3(0, 0, 0));
	CAMERA->setPos(D3DXVECTOR3(0, 8, -15));
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Player2::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Player2::render(void)
{
	// ライフゲージの描画
	f32 gauge = _life / 10.0f;
	PRIMITIVE->drawRect(525, 410, 100*gauge, 20, 0xffff00ff);

	// 球の描画
	PRIMITIVE->drawSphere(_pos, 1.0f, _pTexture, 20, _alpha);
}
