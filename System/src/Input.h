//===========================================================================
//!
//!	@file		XInput.h
//!	@brief		XInputクラス
//!
//===========================================================================
#pragma once

#include <XInput.h>

#pragma comment(lib,"xinput.lib")


//===========================================================================
// XInputクラス
//===========================================================================
class XInput
{
public:
	//-------------------------------------------------------------
	//! ボタン割り当て
	//-------------------------------------------------------------
	enum BUTTON{
		BUTTON_UP				= XINPUT_GAMEPAD_DPAD_UP,
		BUTTON_DOWN				= XINPUT_GAMEPAD_DPAD_DOWN,
		BUTTON_LEFT				= XINPUT_GAMEPAD_DPAD_LEFT,
		BUTTON_RIGHT 			= XINPUT_GAMEPAD_DPAD_RIGHT,
		BUTTON_START 			= XINPUT_GAMEPAD_START,
		BUTTON_BACK				= XINPUT_GAMEPAD_BACK,
		BUTTON_LEFT_THUMB		= XINPUT_GAMEPAD_LEFT_THUMB,
		BUTTON_RIGHT_THUMB		= XINPUT_GAMEPAD_RIGHT_THUMB,
		BUTTON_LEFT_SHOULDER	= XINPUT_GAMEPAD_LEFT_SHOULDER,
		BUTTON_RIGHT_SHOULDER	= XINPUT_GAMEPAD_RIGHT_SHOULDER,
		BUTTON_A 				= XINPUT_GAMEPAD_A,
		BUTTON_B 				= XINPUT_GAMEPAD_B,
		BUTTON_X 				= XINPUT_GAMEPAD_X,
		BUTTON_Y 				= XINPUT_GAMEPAD_Y,
	};

	static const u8 MAX_CONTROLLERS = 4;	//!< 最大コントローラー入力数
	static const f32 INPUT_DEADZONE;		//!< THUMBの数値を検知しない範囲


	//-------------------------------------------------------------
	//!	@name 情報取得
	//-------------------------------------------------------------
	//!@{
	//! コントローラーの情報更新
	void	updateControllerState(void);
	//! ボタンが押されているかチェック
	b32		isButtonPress(u32 no, u32 button);
	//!	ボタンが押されたかチェック
	b32		isButtonPush(u32 no, u32 button);
	//! ボタンが離されたかチェック
	b32		isButtonRelease(u32 no, u32 button);

	//! LeftTriger情報取得
	s32				getLeftTriger(u32 no);
	//! RightTriger情報取得
	s32				getRightTriger(u32 no);
	//! LeftThumbの情報取得
	D3DXVECTOR2		getLeftThumb(u32 no);
	//! LeftThumbの情報取得
	D3DXVECTOR2		getRightThumb(u32 no);
	//!@}

	//! ボタン入力テスト
	void	buttonTest(void);

	static XInput*	getInstance(void);

private:
	//! コンストラクタ
	XInput(void);
	//! デストラクタ
	~XInput(void);

	static XInput _instance;	//!< 唯一のインスタンス

	//-------------------------------------------------------------
	//! コントローラー情報格納用構造体
	//----------------------------------------------覚え0---------------
	struct InputState
	{
		XINPUT_STATE _state;
		u32 _conected;
		u16 _button;			//!< 現在のボタンの状態取得
		u16 _oldButton;		//!< 前回のボタンの状態取得
	};

	InputState	_controllerState[MAX_CONTROLLERS];	//!< コントローラー情報格納用
};

#define IInput (XInput::getInstance())