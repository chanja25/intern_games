//===========================================================================
//!
//!	@file		Player2.h
//!	@brief		Player2クラス
//!
//===========================================================================
#pragma once


//===========================================================================
// Player2クラス
//===========================================================================
class Player2 : public Character
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! コンストラクタ
	Player2(void);
	//! デストラクタ
	~Player2(void);

	//! 初期化
	void	init(void);
	//! 解放
	void	cleanUp(void);
	//!@}

	//! 更新
	void	upDate(void);
	//!	描画
	void	render(void);

private:
	u32	_bulletLevel;	//!< 弾のレベル
};