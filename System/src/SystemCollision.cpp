//===========================================================================
//!
//!	@file		SystemCollision.cpp
//!	@brief		SystemCollisionクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	SystemCollision
//---------------------------------------------------------------------------
SystemCollision	SystemCollision::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemCollision::SystemCollision(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SystemCollision::~SystemCollision(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemCollision::init(void)
{
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SystemCollision::upDate(void)
{
	//	当たり用のリストの中から先頭のノードを取り出す
	Node* pObjectNode = _objectList.getFirstNode();
	Node* pAttackNode;

	//	当たり用オブジェクトが存在すればループする
	while( pObjectNode ) {
		//	当たり用のオブジェクト取得
		Object* pObject = reinterpret_cast<Object*>(pObjectNode->getObject());

		//	攻撃用のリストの中から先頭のノードを取り出す
		pAttackNode = _attackList.getFirstNode();
		//	攻撃用オブジェクトが存在すればループする
		while( pAttackNode ) {
			//	攻撃用のオブジェクト取得
			Object*	pAttack	= reinterpret_cast<Object*>(pAttackNode->getObject());

			//	オブジェクト同士の当たりを取る
			if( collision(pObject, pAttack) ) {
				//	当たっていればステータスをセットにしてループを抜ける
				pObject->setState(pAttack->getAttribute());
				pAttack->setState(pObject->getAttribute());
				break;
			}

			//	次の攻撃用オブジェクトに進む
			pAttackNode = pAttackNode->getNext();
		}

		//	当たり判定が終わったオブジェクトからリストを外し、次のオブジェクトに進む
		_objectList.deleteNode(pObjectNode);

		pObjectNode = _objectList.getFirstNode();
	}
	//	当たり判定が終わったので全ての攻撃オブジェクトを消す
	_attackList.allDeleteNode();
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemCollision::cleanUp(void)
{
	_objectList.allDeleteNode();
	_attackList.allDeleteNode();
}

//---------------------------------------------------------------------------
//	攻撃の当たり用リスト取得
//!	@param	pObject [in] 当たり判定を取るオブジェクト
//---------------------------------------------------------------------------
void	SystemCollision::setObject(Object* pObject)
{
	if( pObject->getHitAttribute() == 0 ) {
		_objectList.addNodeEnd(pObject->getNode());
	} else {
		_attackList.addNodeEnd(pObject->getNode());
	}
}

//---------------------------------------------------------------------------
//	オブジェクト同士の当たり判定
//!	@param	pObject [in] 攻撃を受ける側
//!	@param	pAttack	[in] 攻撃を与える側
//!	@retval	TRUE	当たっている
//!	@retval	FALSE	当たっていない
//---------------------------------------------------------------------------
b32		SystemCollision::collision(SystemCollision::Object *pObject, SystemCollision::Object *pAttack)
{
	//	攻撃の属性が当たり判定を行う属性か確認
	if( pObject->getAttribute() & pAttack->getHitAttribute() ) {

		//	オブジェクトの形が球か調べる
		if( pObject->getShape() == SystemCollision::Object::SHAPE_SPHERE ) {

			//	攻撃の形が球か調べる
			if( pAttack->getShape() == SystemCollision::Object::SHAPE_SPHERE ) {

				//	球と球の当たり判定を行う
				return collision(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getRadius());

			} else {
				//	球とカプセルの当たり判定を行う
				return collisionCapsule(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getPos2(), pAttack->getRadius());
			}

		} else {
			//	攻撃の形が球か調べる
			if( pAttack->getShape() == SystemCollision::Object::SHAPE_SPHERE ) {

				//	球とカプセルの当たり判定を行う
				return collisionCapsule(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getPos2(), pAttack->getRadius());

			} else {

				//	カプセルとカプセルの当たり判定を行う
				//return collision(pObject->getPos(), pObject->getRadius(), pAttack->getPos(), pAttack->getRadius());
			}
		}
	}

	return FALSE;
}

//---------------------------------------------------------------------------
//	球と球の当たり判定
//!	@param pos1		[in] 一つ目の球の中心の位置
//!	@param radius1	[in] 一つ目の球の半径
//!	@param pos2		[in] 二つ目の球の中心の位置
//!	@param radius1	[in] 二つ目の球の半径
//!	@retval	TRUE	当たっている
//!	@retval	FALSE	当たっていない
//---------------------------------------------------------------------------
b32		SystemCollision::collision(D3DXVECTOR3 pos1, f32 radius1, D3DXVECTOR3 pos2, f32 radius2)
{
	//	2点の距離を出す
	D3DXVECTOR3 dist = pos1 - pos2;

	//	2点が半径の和より小さければ当たっている
	return ( D3DXVec3Length(&dist) < (radius1 + radius2) );
}

//---------------------------------------------------------------------------
//	球とカプセルの当たり判定
//!	@param pos		[in] 球の中心の位置
//!	@param radius1	[in] 球の半径
//!	@param pos1		[in] カプセルの一つ目の半球の中心の位置
//!	@param pos2		[in] カプセルの二つ目の半球の中心の位置
//!	@param radius2	[in] カプセルの半径
//!	@retval	TRUE	当たっている
//!	@retval	FALSE	当たっていない
//---------------------------------------------------------------------------
b32		SystemCollision::collisionCapsule(D3DXVECTOR3 pos, f32 radius1, D3DXVECTOR3 pos1, D3DXVECTOR3 pos2, f32 radius2)
{
	D3DXVECTOR3	dist;
	//	球とカプセルの一つ目の半球との距離を出す
	dist = pos - pos1;
	//	2点が半径の和より小さければ当たっている
	if( D3DXVec3Length(&dist) < (radius1 + radius2) ) {
		return TRUE;
	}

	//	球とカプセルの二つ目の半球との距離を出す
	dist = pos - pos2;
	//	2点が半径の和より小さければ当たっている
	if( D3DXVec3Length(&dist) < (radius1 + radius2) ) {
		return TRUE;
	}


	D3DXVECTOR3	vec1, vec2;
	f32	cos, length1, length2;

	//	カプセルの一つ目の半球から球への向きと距離を出す
	vec1 = pos  - pos1;
	length1 = D3DXVec3Length(&vec1);

	//	カプセルの一つ目の半球からカプセルの二つ目の半球への向きと距離を出す
	vec2 = pos2 - pos1;
	length2 = D3DXVec3Length(&vec2);

	//	二つの向きと距離を使ってコサインθを求める
	cos = D3DXVec3Dot(&vec1, &vec2) / (length1 * length2);

	//	コサインθの値が0以下なら鈍角なので当たっていない
	if( cos < 0 ) return FALSE;

	//	カプセルの二つ目の半球から球への向きと距離を出す
	vec1 = pos  - pos2;
	length1 = D3DXVec3Length(&vec1);

	//	カプセルの二つ目の半球からカプセルの一つ目の半球への向きと距離を出す
	vec2 = pos1 - pos2;
	length2 = D3DXVec3Length(&vec2);

	//	二つの向きと距離を使ってコサインθを求める
	cos = D3DXVec3Dot(&vec1, &vec2) / (length1 * length2);

	
	//	コサインθの値が0以下なら鈍角なので当たっていない
	if( cos < 0 ) return FALSE;

	//	コサインθのからサインθの値を求める
	f32 sin = sqrtf(1 - cos*cos);

	//	2点の距離を出して、半径の和と比較する。
	//	半径の和より近ければ当たっている
	return ( (sin * length1) < (radius1 + radius2) );
}

//---------------------------------------------------------------------------
//	インスタンスの取得
//!	@retval	インスタンス
//---------------------------------------------------------------------------
SystemCollision*	SystemCollision::getInstance(void)
{
	return &_instance;
}



//===========================================================================
//
//		Objectクラス(SystemCollisionのインナークラス)
//
//===========================================================================

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemCollision::Object::Object(void)
{
	_state	= FALSE;
	_node.setObject(this);
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SystemCollision::Object::~Object(void)
{
}

//---------------------------------------------------------------------------
//	オブジェクト当たりのセット
//!	@param	shape		 [in] 形
//!	@param	attribute	 [in] 自分の属性
//!	@param	hitAttribute [in] 当てる事ができる属性
//---------------------------------------------------------------------------
void	SystemCollision::Object::init(u32 shape, u32 attribute, u32 hitAttribute)
{
	_shape			= shape;
	_attribute		= attribute;
	_hitAttribute	= hitAttribute;
}

//---------------------------------------------------------------------------
//	オブジェクト当たりのセット
//!	@param	pos		[in] 位置
//!	@param	radius	[in] 半径
//!	@param	pos2	[in] 位置2(カプセルの場合使う)
//---------------------------------------------------------------------------
void	SystemCollision::Object::setObject(D3DXVECTOR3 pos, f32 radius, D3DXVECTOR3 pos2)
{
	_state		= FALSE;
	_pos		= pos;
	_pos2		= pos2;
	_radius		= radius;
}

//---------------------------------------------------------------------------
//	オブジェクトの位置を取得
//!	@retval	位置
//---------------------------------------------------------------------------
D3DXVECTOR3	SystemCollision::Object::getPos(void)
{
	return _pos;
}

//---------------------------------------------------------------------------
//	オブジェクトの位置2を取得
//!	@retval	位置2
//---------------------------------------------------------------------------
D3DXVECTOR3	SystemCollision::Object::getPos2(void)
{
	return _pos2;
}

//---------------------------------------------------------------------------
//	オブジェクトの半径を取得
//!	@retval	半径
//---------------------------------------------------------------------------
f32		SystemCollision::Object::getRadius(void)
{
	return _radius;
}

//---------------------------------------------------------------------------
//	オブジェクトのステータスセット
//!	@param	state [in] ステータス
//---------------------------------------------------------------------------
void	SystemCollision::Object::setState(u32 state)
{
	_state = state;
}

//---------------------------------------------------------------------------
//	オブジェクトのステータス
//!	@retval	ステータス
//---------------------------------------------------------------------------
u32		SystemCollision::Object::getState(void)
{
	return _state;
}

//---------------------------------------------------------------------------
//	形の取得
//!	@retval	形
//---------------------------------------------------------------------------
u32		SystemCollision::Object::getShape(void)
{
	return _shape;
}

//---------------------------------------------------------------------------
//	属性の取得
//!	@retval	属性
//---------------------------------------------------------------------------
u32		SystemCollision::Object::getAttribute(void)
{
	return _attribute;
}

//---------------------------------------------------------------------------
//	当てる属性の取得
//!	@retval	属性
//---------------------------------------------------------------------------
u32		SystemCollision::Object::getHitAttribute(void)
{
	return _hitAttribute;
}

//---------------------------------------------------------------------------
//	ノードの取得
//!	@retval	ノード
//---------------------------------------------------------------------------
Node*	SystemCollision::Object::getNode(void)
{
	return &_node;
}