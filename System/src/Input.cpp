//===========================================================================
//!
//!	@file		XInput.h
//!	@brief		XInputクラス
//!
//===========================================================================
#include "SystemStdafx.h"



//---------------------------------------------------------------------------
//	インスタンスの生成
//---------------------------------------------------------------------------
XInput XInput::_instance;

//	HUMBの数値を検知しない範囲の設定
const f32 XInput::INPUT_DEADZONE = 0.25f;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
XInput::XInput(void)
{
	// コントローラーの情報初期化
	ZeroMemory(_controllerState, sizeof(InputState)*MAX_CONTROLLERS );

	for( DWORD i=0; i<MAX_CONTROLLERS; i++ ) {
		_controllerState[i]._button		= 0;
		_controllerState[i]._oldButton	= 0;
	}
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
XInput::~XInput(void)
{
}

//---------------------------------------------------------------------------
//	コントローラーの情報更新
//---------------------------------------------------------------------------
void	XInput::updateControllerState(void)
{
	DWORD result;


	// コントローラーの最大数ループ
	for( DWORD i=0; i<MAX_CONTROLLERS; i++ ){
		// コントローラーの情報取得
		result = XInputGetState( i, &_controllerState[i]._state );

		if( result == ERROR_SUCCESS ){
			// コントローラーがあればデータ取得
			_controllerState[i]._conected	= true;
			_controllerState[i]._oldButton	= _controllerState[i]._button;
			_controllerState[i]._button		= _controllerState[i]._state.Gamepad.wButtons;
		}else {
			// 無ければfalseにする
			_controllerState[i]._conected	= false;
			_controllerState[i]._oldButton	= 0;
			_controllerState[i]._button		= 0;
		}
	}
}

//---------------------------------------------------------------------------
//	ボタンが押されているかチェック
//! @param	no		[in] 情報を取得したいコントローラーの割り当て番号
//! @param	button	[in] 情報を取得したいキーの割り当て番号
//! @retval	true	入力されている
//! @retval	false	入力されていない
//---------------------------------------------------------------------------
b32		XInput::isButtonPress(u32 no, u32 button)
{
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return false;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return false;

	// コントローラーのボタン情報取得
	return ( _controllerState[no]._button & button );
}

//---------------------------------------------------------------------------
//	ボタンが押されたかチェック
//! @param	no		[in] 情報を取得したいコントローラーの割り当て番号
//! @param	button	[in] 情報を取得したいキーの割り当て番号
//! @retval	true	入力された
//! @retval	false	入力されていない
//---------------------------------------------------------------------------
b32		XInput::isButtonPush(u32 no, u32 button)
{
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return false;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return false;

	// コントローラーのボタン情報取得
	return ( (_controllerState[no]._button ^ _controllerState[no]._oldButton & _controllerState[no]._button) & button );
}

//---------------------------------------------------------------------------
//	ボタンが離されたかチェック
//! @param	no		[in] 情報を取得したいコントローラーの割り当て番号
//! @param	button	[in] 情報を取得したいキーの割り当て番号
//! @retval true	離された
//! @retval false	離されていない
//---------------------------------------------------------------------------
b32		XInput::isButtonRelease(u32 no, u32 button)
{
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return false;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return false;

	// コントローラーのボタン情報取得
	return ( (_controllerState[no]._oldButton ^ _controllerState[no]._button & _controllerState[no]._oldButton) & button );
}

//---------------------------------------------------------------------------
//	LeftTriger情報取得
//! @param	no [in] 情報を取得したいコントローラーの割り当て番号
//! @retval	LeftTrigerの入力状況
//---------------------------------------------------------------------------
s32		XInput::getLeftTriger(u32 no)
{
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return false;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return false;

	// コントローラーのボタン情報取得
	return _controllerState[no]._state.Gamepad.bLeftTrigger;
}

//---------------------------------------------------------------------------
//	RightTriger情報取得
//! @param	no [in] 情報を取得したいコントローラーの割り当て番号
//! @retval	RightTrigerの入力状況
//---------------------------------------------------------------------------
s32		XInput::getRightTriger(u32 no)
{
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return false;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return false;

	// コントローラーのボタン情報取得
	return _controllerState[no]._state.Gamepad.bRightTrigger;
}

//---------------------------------------------------------------------------
//	LeftThumbの情報取得
//! @param	no [in] 情報を取得したいコントローラーの割り当て番号
//! @retval	LeftThumbの入力状況(D3DXVECTOR2)
//---------------------------------------------------------------------------
D3DXVECTOR2		XInput::getLeftThumb(u32 no)
{
	D3DXVECTOR2 thumb(0,0);
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return thumb;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return thumb;

	// コントローラーのボタン情報取得
	thumb.x = static_cast<f32>(_controllerState[no]._state.Gamepad.sThumbLX) / static_cast<f32>(0x7fff);
	thumb.y = static_cast<f32>(_controllerState[no]._state.Gamepad.sThumbLY) / static_cast<f32>(0x7fff);

	// 数値の切捨て
	if( thumb.x < INPUT_DEADZONE &&
		thumb.x > -INPUT_DEADZONE &&
		thumb.y < INPUT_DEADZONE &&
		thumb.y > -INPUT_DEADZONE ) {
			thumb.x = 0;
			thumb.y = 0;
	}

	return thumb;
}


//---------------------------------------------------------------------------
//	LeftThumbの情報取得
//! @param	no [in] 情報を取得したいコントローラーの割り当て番号
//! @retval	RightThumbの入力状況(D3DXVECTOR2)
//---------------------------------------------------------------------------
D3DXVECTOR2		XInput::getRightThumb(u32 no)
{
	D3DXVECTOR2 thumb(0,0);
	// コントローラーの最大数以内でなければエラー
	if( no >= MAX_CONTROLLERS ) {
		ASSERT(false, "コントローラーの最大数以内を指定してください");
		return thumb;
	}

	// コントローラーの有無確認
	if( !_controllerState[no]._conected ) return thumb;

	// コントローラーのボタン情報取得
	thumb.x = static_cast<f32>(_controllerState[no]._state.Gamepad.sThumbRX) / (0x7fff);
	thumb.y = static_cast<f32>(_controllerState[no]._state.Gamepad.sThumbRY) / (0x7fff);

	// 数値の切捨て
	if( thumb.x < INPUT_DEADZONE &&
		thumb.x > -INPUT_DEADZONE &&
		thumb.y < INPUT_DEADZONE &&
		thumb.y > -INPUT_DEADZONE ) {
			thumb.x = 0;
			thumb.y = 0;
	}

	return thumb;
}

//---------------------------------------------------------------------------
//	ボタンの入力テスト
//---------------------------------------------------------------------------
void	XInput::buttonTest(void)
{
	u16	buttonsState;
	for( DWORD i=0; i<MAX_CONTROLLERS; i++ ) {
		if( !_controllerState[i]._conected ) continue;
		 buttonsState = _controllerState[i]._state.Gamepad.wButtons;

		FONT->drawFont( 0, 50, 0xffffffff,
			"Contoroller :%d\n"
			"Button :%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n"
			"LeftTriger :%d\n"
			"RightTriger :%d\n"
			"LeftThumb :%1._2f/%1._2f\n"
			"RightThumb :%1._2f/%1._2f\n",
			i,
			isButtonPress(i, XINPUT_GAMEPAD_DPAD_UP) ? L"DPAD_UP " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_DPAD_DOWN) ? L"DPAD_DOWN " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_DPAD_LEFT) ? L"DPAD_LEFT " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_DPAD_RIGHT) ? L"DPAD_RIGHT " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_START) ? L"START " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_BACK) ? L"BACK " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_LEFT_THUMB) ? L"LEFT_THUMB " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_RIGHT_THUMB) ? L"RIGHT_THUMB " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_LEFT_SHOULDER) ? L"LEFT_SHOULDER " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_RIGHT_SHOULDER) ? L"RIGHT_SHOULDER " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_A) ? L"A " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_B) ? L"B " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_X) ? L"X " : L"",
			isButtonPress(i, XINPUT_GAMEPAD_Y) ? L"Y " : L"",
			getLeftTriger( i ),
			getRightTriger( i ),
			getLeftThumb( i ).x,
			getLeftThumb( i ).y,
			getRightThumb( i ).x,
			getRightThumb( i ).y );
	}
}


//---------------------------------------------------------------------------
//	インタンスのゲット関数
//!	@retval	インスタンス
//---------------------------------------------------------------------------
XInput*	XInput::getInstance(void)
{
	return &_instance;
}
