//===========================================================================
//!
//!	@file		SystemTask.h
//!	@brief		SystemTaskクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// SystemTaskクラス
//===========================================================================
class SystemTask : public TaskBase
{
public:
	//-------------------------------------------------------------
	//! リストの種類
	//-------------------------------------------------------------
	enum LIST {
		LIST_PLAYER,
		LIST_ENEMY,
		LIST_BULLET,
		LIST_MAX,
	};

	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! 初期化
	void	init(void);
	//! 更新
	void	upDate(void);
	//! 解放
	void	cleanUp(void);
	//!@}

	//-------------------------------------------------------------
	//!	@name タスク管理
	//-------------------------------------------------------------
	//!@{
	//! 対象ラインの更新
	void	upDate(List* pList);
	//! 描画
	void	render(void);
	//! 対象ラインの描画
	void	render(List* pList);
	//! 全ラインの消去
	void	killAll(void);
	//! 対象のラインの消去
	void	killTask(TaskBase* pLine);
	//!	対象のタスクリストの末尾に、タスク追加
	void	addBottom(TaskBase* pTask, u32 list);
	//!	対象のリストを取得する
	List*	getList(u32 list);
	//!@}


	//! インスタンスの取得
	static	SystemTask*	getInstance(void);

private:
	//! コンストラクタ
	SystemTask();
	//! デストラクタ
	~SystemTask(){};

	static	SystemTask _instance;	//!< インスタンス

	List*	_pList;					//!< リストポインタ
};

//! SYSTEMTASKの取得関数のマクロ
#define	TASK	(SystemTask::getInstance())