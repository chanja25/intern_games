//===========================================================================
//!
//!	@file		SystemTask.cpp
//!	@brief		SystemTaskクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	SystemTask
//---------------------------------------------------------------------------
SystemTask	SystemTask::_instance;


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SystemTask::SystemTask(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SystemTask::init(void)
{
	// リストを必要分の生成
	_pList = new List[LIST_MAX];

	ZeroMemory(_pList, sizeof(List)*LIST_MAX);

	_attribute = ATT_UPDATA | ATT_RENDER;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SystemTask::upDate(void)
{
	if( !(_attribute & ATT_UPDATA) ) return;

	//	リストが存在する分だけ回す
	for( u32 i=0; i<LIST_MAX; i++ ) {
		//	リストの更新
		upDate(&_pList[i]);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SystemTask::cleanUp(void)
{
	// ラインを全て解放
	killAll();

	// リストの解放
	SAFE_DELETES(_pList);
}

//---------------------------------------------------------------------------
//	対象ラインの更新
//!	@param	pList [in] 更新するリスト
//---------------------------------------------------------------------------
void	SystemTask::upDate(List* pList)
{
	//	リストの最初のノードを取得しループさせる
	Node* pNode = pList->getFirstNode();
	while( pNode ) {
		//	次のノードを取得しておく
		Node*	pNext = pNode->getNext();
		//	タスクを取り出す
		TaskBase* pTask = reinterpret_cast<TaskBase*>(pNode->getObject());
		switch( pTask->getState() ) {
			case STATE_SETUP:
				//	初期化
				pTask->init();
				//break;
			case STATE_RUNNING:
				//	更新するか確認
				if( !(pTask->getAttribute() & ATT_UPDATA) ) break;
				pTask->upDate();
				break;
			case STATE_INVALID:
				//	削除要請
				pTask->setState(STATE_DELETE);
				break;
			case STATE_DELETE:
				//	削除
				killTask(pTask);
				break;

			default:
				//	デフォルトならエラー
				ASSERT(false, "タスクエラーです");
				break;
		}
		//	次のノードへ進む
		pNode = pNext;
	}
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	SystemTask::render(void)
{
	if( !(_attribute & ATT_RENDER) ) return;

	//	リストが存在する分だけ回す
	for( u32 i=0; i<LIST_MAX; i++ ) {
		//	リスト毎の描画
		render(&_pList[i]);
	}
}

//---------------------------------------------------------------------------
//	対象ラインの描画
//!	@param	pList [in] 描画するリスト
//---------------------------------------------------------------------------
void	SystemTask::render(List* pLine)
{
	//	最初のノードを取得しループ
	Node* pNode = pLine->getFirstNode();
	while( pNode ) {
		//	タスクを取り出す
		TaskBase* pTask = reinterpret_cast<TaskBase*>(pNode->getObject());

		//	タスクが生きているか確認する
		if( pTask->isAlive() ) {
			// 描画するか確認して描画
			if( pTask->getAttribute() & ATT_RENDER ) {
				pTask->render();
			}
		}

		//	次のノードへ進む
		pNode = pNode->getNext();
	}
}

//---------------------------------------------------------------------------
//	全ラインの消去
//---------------------------------------------------------------------------
void	SystemTask::killAll(void)
{
	//---------------------------------------------------------------------------
	//	リストが存在する分だけ回す
	//---------------------------------------------------------------------------
	for( u32 i=0; i<LIST_MAX; i++ ) {
		//	最初のノードを取得しループ
		Node* pNode = _pList[i].getFirstNode();
		while( pNode ) {
			//	次のノードを取得しておく
			Node*	pNext = pNode->getNext();

			//	タスクを取り出し削除
			TaskBase* pTask = reinterpret_cast<TaskBase*>(pNode->getObject());
			killTask(pTask);

			//	次のノードへ進む
			pNode = pNext;
		}
	}
}

//---------------------------------------------------------------------------
//	対象タスクの消去
//!	@param	pTask [in] 削除するタスク
//---------------------------------------------------------------------------
void	SystemTask::killTask(TaskBase* pTask)
{
	//	自分が所属しているリストの取得
	List*	pList = pTask->getNode()->getList();

	//	ラインをリストから削除
	pList->deleteNode(pTask->getNode());

	//	ラインの解放
	pTask->cleanUp();
	SAFE_DELETE(pTask);
}

//---------------------------------------------------------------------------
//	対象のタスクリストの末尾に、タスク追加
//!	@param	pTask	[in] 追加するタスク
//!	@param	list	[in] 追加するタスクの種類
//---------------------------------------------------------------------------
void	SystemTask::addBottom(TaskBase* pTask, u32 list)
{
	if( list >= LIST_MAX ) {
		ASSERT(false, "リストの要素数を超えています");
	}

	//	タスク追加
	_pList[list].addNodeEnd(pTask->getNode());
}

//---------------------------------------------------------------------------
//	対象リストの取得
//!	@param	list	[in] 取得するリストの種類
//---------------------------------------------------------------------------
List*	SystemTask::getList(u32 list)
{
	return &_pList[list];
}

//---------------------------------------------------------------------------
//	インスタンスの取得
//!	@retval	インスタンス
//---------------------------------------------------------------------------
SystemTask*	SystemTask::getInstance(void)
{
	return &_instance;
}