//===========================================================================
//!
//!	@file		System.cpp
//!	@brief		システムライブラリ
//!
//===========================================================================
#include "SystemStdafx.h"

//===========================================================================
// System
//===========================================================================
System*		System::_pInstance = NULL;

//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
System::System(void) :
_hwnd(NULL)
{
	ASSERT(_pInstance == NULL, "instance exist.");
	_pInstance = this;
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
System::~System(void)
{
	_pInstance = NULL;
}

//---------------------------------------------------------------------------
//	初期化
//!	@retval	true	成功
//!	@retval	false	失敗
//---------------------------------------------------------------------------
bool System::_initSystem(void)
{
	//-------------------------------------------------------------
	// ウィンドウクラス登録
	//-------------------------------------------------------------
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= System::windowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= GetModuleHandle(NULL);
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "System HEXADRIVE-sample";
	wcex.hIconSm		= NULL;
	// ウィンドウクラス登録
	if( RegisterClassEx(&wcex) == 0 ) return false;

	//-------------------------------------------------------------
	// ウィンドウ作成
	//-------------------------------------------------------------
	HWND	hwnd = CreateWindow(wcex.lpszClassName,
								"川本 恵史",
								WS_OVERLAPPEDWINDOW,
								CW_USEDEFAULT, 0,
								640, 480,
								NULL,
								NULL,
								GetModuleHandle(NULL),
								NULL);
	if( hwnd == NULL ) return false;

	//---- 表示ON再描画
	ShowWindow(hwnd, SW_SHOW);
	UpdateWindow(hwnd);
	_hwnd = hwnd;				// 保存

	srand((u32)time(NULL));

	//	DirectXクラスの初期化
	DIRECTX->init(_hwnd);

	//	フォントクラスの初期化
	FONT->init(DIRECTX->getD3DDevice());

	//	タスクシステムの初期化
	TASK->init();

	//	システムリソースの初期化
	RESOURCE->init();

	//	システムプリミティブの初期化
	PRIMITIVE->init();

	//	カメラの初期化
	CAMERA->init();

	//	デバッグカメラの初期化
	DEBUGCAMERA->init();

	//	システムシーンの初期化
	SCENE->init();

	// プロファイラの初期化
	#if USE_PROFILER
		if( ! Profiler::init() ) {
			return false;
		}
	#endif //~#if USE_PROFILER

	return true;
}


#if USE_PROFILER

// バーの長さ
static const f32 BAR_LENGTH = 600.0f;
// 目標フレームレート
static const u32 TARGET_FPS = 60;

/**
 * プロセスバーを描画する
 */
void drawProcessBar(Profiler::Log* pLog, f32 drawPos)
{
	// 目標フレームレートの１フレームの時間(ns)
	static u32 totalNanoSec = (u32)(Timer::TICK_DELTA / TARGET_FPS);

	while( pLog ) {

		// -*- 表示に使用できるパラメータ -*-
//		char buf[1024];
//		sprintf_s(buf, sizeof(buf), "_pName(%s)\n", pLog->_pName);					// プロファイル名
//		OutputDebugString(buf);
//		sprintf_s(buf, sizeof(buf), "	_totalTime(%u)\n", pLog->_totalTime);		// トータル処理時間
//		OutputDebugString(buf);
//		sprintf_s(buf, sizeof(buf), "	_count(%u)\n", pLog->_count);				// 呼び出し回数
//		OutputDebugString(buf);
//		sprintf_s(buf, sizeof(buf), "	_averageTime(%u)\n", pLog->_averageTime);	// 平均処理時間
//		OutputDebugString(buf);
//		sprintf_s(buf, sizeof(buf), "	_minTime(%u)\n", pLog->_minTime);			// 最小処理時間
//		OutputDebugString(buf);
//		sprintf_s(buf, sizeof(buf), "	_maxTime(%u)\n", pLog->_maxTime);			// 最大処理時間
//		OutputDebugString(buf);
		// ----------------------------------

		// トータルの処理時間からバーの長さを決定
		f32 barLength = ((f32)pLog->_totalTime / (f32)totalNanoSec * BAR_LENGTH);

		// バーの色をセット
		D3DCOLOR d3dColor = D3DCOLOR_RGBA(0xff, 0xff, 0xff, 0xff);
		switch(pLog->_color) {
		case Profiler::COLOR_WHITE:
			d3dColor = D3DCOLOR_RGBA(0xff, 0xff, 0xff, 0xff);
			break;
		case Profiler::COLOR_RED:
			d3dColor = D3DCOLOR_RGBA(0xff, 0x00, 0x00, 0xff);
			break;
		case Profiler::COLOR_GREEN:
			d3dColor = D3DCOLOR_RGBA(0x00, 0xff, 0x00, 0xff);
			break;
		case Profiler::COLOR_BLUE:
			d3dColor = D3DCOLOR_RGBA(0x00, 0x00, 0xff, 0xff);
			break;
		case Profiler::COLOR_YELLOW:
			d3dColor = D3DCOLOR_RGBA(0xff, 0xff, 0x00, 0xff);
			break;
			break;
		case Profiler::COLOR_BLACK:
			d3dColor = D3DCOLOR_RGBA(0x00, 0x00, 0x00, 0xff);
			break;
		}

		// バーの描画
		// TODO : drawPos から処理棒の長さ(barLength) 分の棒を指定の色(d3dColor) で表示
		PRIMITIVE->drawRect(drawPos, 20, barLength, 10, d3dColor);

		// 再帰的に子のログ情報を基に描画
		if( pLog->_pChild ) {
			drawProcessBar(pLog->_pChild, drawPos);
		}

		drawPos += barLength;

		pLog = pLog->_pNext;
	}
}

/**
 * プロセスバーを描画する
 */
void drawProcessBar(void)
{
	// プロセスバーの描画
	Profiler::Log* pLog = Profiler::getInstancePtr()->getRootLog();
	if( pLog->_pChild ) {
		drawProcessBar(pLog->_pChild, 10.0f);
	}

	// バーのアウトラインを描画
	// TODO : BAR_LENGTH を使用したバーのアウトラインを表示
	PRIMITIVE->drawRect(10.0f, 18, BAR_LENGTH, 2, 0xffffffff);
}

#endif //~#if USE_PROFILER


//---------------------------------------------------------------------------
//	メインループ
//!	@retval	true	処理続行
//!	@retval	false	処理終了
//---------------------------------------------------------------------------
int	System::mainLoop(void)
{
	//-------------------------------------------------------------
	// システム初期化
	//-------------------------------------------------------------
	if( !_initSystem() ) return 0;

	init();				// 初期化通知

	//-------------------------------------------------------------
	// メインメッセージループ
	//-------------------------------------------------------------
	MSG	msg;

	for(;;) {
		if( PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) ){
			if( !GetMessage(&msg, NULL, 0, 0) ) {	// メッセージ処理
				break;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			// プロファイラにフレームの開始を通知
			#if USE_PROFILER
				Profiler::getInstancePtr()->startFrame();
			#endif //~#if USE_PROFILER

			BEGIN_PROFILE("test01", Profiler::COLOR_WHITE);
			{
				BEGIN_PROFILE("test02", Profiler::COLOR_WHITE);
				// キー情報更新
				IInput->updateControllerState();
				END_PROFILE("test02");

				//---------------------------------------------------------------------------
				// 描画開始
				//---------------------------------------------------------------------------
				DIRECTX->beginScene();

				// シーンの更新
				SCENE->upDate();

				//	タスクシステムの更新、描画
				BEGIN_PROFILE("test04", Profiler::COLOR_YELLOW);
				TASK->upDate();
				TASK->render();
				END_PROFILE("test04");
				//	システムプリミティプの描画
				for( u32 i=0; i<2; i++ )
					PRIMITIVE->render();

				//	カメラの更新
				CAMERA->upDate();
			}
			END_PROFILE("test01");

			{
				PROFILE("test06", Profiler::COLOR_RED);
				#if USE_PROFILER
					// プロセスバーの描画
					drawProcessBar();
				#endif
			}
			//---------------------------------------------------------------------------
			// 描画終了
			//---------------------------------------------------------------------------
			DIRECTX->endScene();

			move();		// 更新通知
		}
	}
	//-------------------------------------------------------------
	// システム解放
	//-------------------------------------------------------------
	cleanup();				// 解放通知
	_cleanupSystem();

	return (int)msg.wParam;
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void System::_cleanupSystem(void)
{
	// プロファイラの解放
	#if USE_PROFILER
		Profiler::cleanup();
	#endif //~#if USE_PROFILER

	//	システムシーンの解放
	SCENE->cleanUp();

	//	カメラの解放
	CAMERA->cleanUp();

	//	デバッグカメラの解放
	DEBUGCAMERA->cleanUp();

	//	システムプリミティブの解放
	PRIMITIVE->cleanUp();

	//	システムリソースの初期化
	RESOURCE->cleanUp();

	// システムタスクのラインを全て解放
	TASK->cleanUp();

	//	フォントクラスの解放
	FONT->cleanUp();

	//	DirectXクラスの開放
	DIRECTX->cleanUp();
}

//---------------------------------------------------------------------------
//	ウィンドウプロシージャ
//!	@param	hwnd	[in]	ウィンドウハンドル
//!	@param	message	[in]	ウィンドウメッセージ
//!	@param	wparam	[in]	パラメータ引数１
//!	@param	lparam	[in]	パラメータ引数２
//!	@retval	メッセージ処理の結果（メッセージに依存）
//---------------------------------------------------------------------------
LRESULT CALLBACK System::windowProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch( message ) {
	//-------------------------------------------------------------
	case WM_PAINT:		// 再描画
	//-------------------------------------------------------------
		{
			PAINTSTRUCT	ps;
			HDC			hdc = BeginPaint(hwnd, &ps);
			// 描画コードをここに追加してください
			EndPaint(hwnd, &ps);
		}
		return 0;
	//-------------------------------------------------------------
	case WM_DESTROY:	//	ウィンドウ破棄
	//-------------------------------------------------------------
		PostQuitMessage(0);
		return 0;
	}

	//---- デフォルトのウィンドウ処理
	return DefWindowProc(hwnd, message, wparam, lparam);
}


//===========================================================================
//	END OF FILE
//===========================================================================
