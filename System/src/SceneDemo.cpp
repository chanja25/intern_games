//===========================================================================
//!
//!	@file		SceneDemo.cpp
//!	@brief		SceneDemoクラス
//!
//===========================================================================
#include "stdafx.h"


//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
SceneDemo::SceneDemo(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
SceneDemo::~SceneDemo(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	SceneDemo::init(void)
{
	DIRECTX->createVertex(&_pVB, 5.0f, 5.0f);

	_pTexture = RESOURCE->loadTexture("Demo.png");

	CAMERA->setPos(D3DXVECTOR3(0, 4, -5));
	CAMERA->setTarget(D3DXVECTOR3(0, 2, 0));

	_y = -8.0f;
	_count = 1080;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	SceneDemo::upDate(void)
{
	// テクスチャの向きと位置の設定
	D3DXMATRIX mat, rot, trans;
	D3DXMatrixTranslation(&trans, 0, _y, 0);

	D3DXMatrixRotationY(&rot, D3DX_PI/180 * _count);
	mat = rot * trans;
	PRIMITIVE->drawTexture(D3DXVECTOR3(0, 2, 0), &mat, _pVB, _pTexture);
	//	裏にも表示させる
	D3DXMatrixRotationY(&rot, D3DX_PI/180 * _count + D3DX_PI);
	mat = rot * trans;
	PRIMITIVE->drawTexture(D3DXVECTOR3(0, 2, 0), &mat, _pVB, _pTexture);

	//	画面の下方から中央に移動させる
	_y = ( _y < 0 )? _y + 0.1f: _y;

	//	ボタンを押すと次のシーンへ移行
	if( IInput->isButtonPush(0, XInput::BUTTON_START) ) {
		SCENE->jumpScene("SceneTitle");
	} else {
		//	しばらく放置しているとタイトルへ移行する
		if( (--_count==0) ) {
			SCENE->jumpScene("SceneTitle");
		}
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	SceneDemo::cleanUp(void)
{
	// テクスチャーの解放
	RESOURCE->releaseTexture(_pTexture);

	// 頂点情報解放
	SAFE_RELEASE(_pVB);
}