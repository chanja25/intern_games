//===========================================================================
//!
//!	@file		Enemy.cpp
//!	@brief		Enemyクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Enemy::Enemy(void)
{
}

//---------------------------------------------------------------------------
//	引数付きコンストラクタ
//!	@param pos	[in] 初期位置
//!	@param life	[in] 体力
//---------------------------------------------------------------------------
Enemy::Enemy(D3DXVECTOR3 pos, s32 life)
{
	_pos = pos;
	_life = life;
}

//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Enemy::~Enemy(void)
{
}

//---------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------
void	Enemy::init(void)
{
	_pTexture = RESOURCE->loadTexture("Enemy.bmp");

	// 敵の初期設定
	_shootCount  = (rand()%20+5) * 20;
	_moveCount	 = 90;
	_countSpeed = 1;

	// ノードの設定
	_node.setObject(this);

	// 当たり判定用オブジェクトの初期化
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_ENEMY );

	// タスクの設定
	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy::upDate(void)
{
	// 弾に当たればライフを減らす
	if( _object.getState() ) {
		_life--;
	}

	//	移動
	_pos.x += cosf(_moveCount*(D3DX_PI/90)) * 0.3f;

	_moveCount += _countSpeed;

	// 弾の制御
	if( _shootCount-- == 0 ) {
		_shootCount = 120;
		Bullet* pBullet = new Bullet();
		pBullet->setState(_pos, D3DXVECTOR3(0, 0, -1), SystemCollision::Object::ATT_ENEMY_BULLET, SystemCollision::Object::ATT_PLAYER);
		TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
	}

	// ライフが無くなれば削除要請、あれば当たり判定をつける
	if( _life <= 0 ) {
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 1.5f);
		COLLISION->setObject(&_object);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Enemy::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy::render(void)
{
	// 球の描画
	PRIMITIVE->drawSphere(_pos, 1.5f, _pTexture, 10, _alpha);
}




