//===========================================================================
//!
//!	@file		DebugCamera.h
//!	@brief		DebugCameraクラス
//!
//===========================================================================
#pragma once

//===========================================================================
// デバッグ用カメラクラス
//===========================================================================
class DebugCamera
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! 初期化
	void init(void);
	//! 更新
	void upDate(void);
	//! 解放
	void cleanUp(void);

	//! カメラのリセット
	void	reset(void);
	//!@}

	//-------------------------------------------------------------
	//!	@name 取得、設定
	//-------------------------------------------------------------
	//!@{
	//! カメラのポジション設定
	void		setPos(D3DXVECTOR3 pos) { _pos = pos; };
	//!	カメラのポジション取得
	D3DXVECTOR3	getPos(void) { return _pos; };

	//! カメラのターゲット設定
	void		setTarget(D3DXVECTOR3 target) { _target = target; };
	//!	カメラのターゲット取得
	D3DXVECTOR3	getTarget(void) { return _target; };

	//!	ワールド,　ビュー座標の変換の取得
	D3DXMATRIX	getView(void)		{ return _view; };
	//!	ビュー、スクリーン座標の変換の取得
	D3DXMATRIX	getProjection(void)	{ return _projection; };
	//!@}

	//! インスタンスの取得
	static	DebugCamera*	getInstance(void);

private:
	//! コンストラクタ
	DebugCamera(void);
	//! デストラクタ
	~DebugCamera(void);

	static DebugCamera _instance;	//!< 唯一のインスタンス


	D3DXVECTOR3	_pos;			//!< ポジション
	D3DXVECTOR3 _target;		//!< ターゲット
	D3DXVECTOR3 _vec;			//!< ターゲットまでの距離と向き


	D3DXMATRIX	_view;			//!< ワールド,　ビュー座標の変換マトリックス
	D3DXMATRIX	_projection;	//!< ビュー、スクリーン座標の変換マトリックス
};

//! デバッグカメラの取得関数のマクロ
#define	DEBUGCAMERA	(DebugCamera::getInstance())