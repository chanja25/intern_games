//===========================================================================
//!
//!	@file		typedef.h
//!	@brief		型宣言
//!
//===========================================================================

//-------------------------------------------------------------
//! 変数型宣言
//-------------------------------------------------------------
typedef	  signed char		s8;		//!< 符号あり  8bit整数
typedef	unsigned char		u8;		//!< 符号なし  8bit整数
typedef	  signed short		s16;	//!< 符号あり 16bit整数
typedef	unsigned short		u16;	//!< 符号なし 16bit整数

typedef	  signed int		s32;	//!< 符号あり 32bit整数
typedef	unsigned int		u32;	//!< 符号なし 32bit整数
typedef	  signed __int64	s64;	//!< 符号あり 64bit整数
typedef	unsigned __int64	u64;	//!< 符号なし 64bit整数

typedef float				f32;	//!< 単精度浮動少数点数
typedef double				f64;	//!< 倍精度浮動少数点数
typedef	BOOL				b32;	//!< 32bitフラグ

// メッセージ付条件アサーション
#define	ASSERT(__CONDITION__,__MESSAGE__)	assert(__CONDITION__ && __MESSAGE__)

//===========================================================================
//	END OF FILE
//===========================================================================
