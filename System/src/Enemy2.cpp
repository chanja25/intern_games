//===========================================================================
//!
//!	@file		Enemy2.cpp
//!	@brief		Enemy2クラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Enemy2::Enemy2(void)
{
}

//---------------------------------------------------------------------------
//	引数付きコンストラクタ
//!	@param pos	[in] 初期位置
//!	@param life	[in] 体力
//---------------------------------------------------------------------------
Enemy2::Enemy2(D3DXVECTOR3 pos, s32 life)
{
	_pos = pos;
	_life = life;
}

//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Enemy2::~Enemy2(void)
{
}

//---------------------------------------------------------------------------
// 初期化
//---------------------------------------------------------------------------
void	Enemy2::init(void)
{
	_pTexture = RESOURCE->loadTexture("Enemy2.bmp");

	// 敵の初期設定
	_shootCount = (rand()%20+5) * 20;
	_moveCount	= 90;
	_countSpeed	= 1;

	// ノードの設定
	_node.setObject(this);

	// 当たり判定用オブジェクトの初期化
	_object.init(	SystemCollision::Object::SHAPE_SPHERE,
					SystemCollision::Object::ATT_ENEMY );

	// タスクの設定
	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy2::upDate(void)
{
	_alpha = 1.0f;

	// 弾に当たればライフを減らす
	if( _object.getState() ) {
		_life--;
		_alpha = 0.1f;
	}

	//	移動
	_pos.x += sinf(_moveCount*(D3DX_PI/180)) * .5f;

	_moveCount += _countSpeed;

	// 弾の制御
	if( --_shootCount == 0 ) {
		_shootCount = 100;
		Bullet2* pBullet = new Bullet2();
		pBullet->setState(_pos, D3DXVECTOR3(0, 0, -1), SystemCollision::Object::ATT_ENEMY_BULLET2, SystemCollision::Object::ATT_PLAYER);
		TASK->addBottom(pBullet, SystemTask::LIST_BULLET);
	}

	// ライフが無くなれば削除要請、あれば当たり判定をつける
	if( _life <= 0 ) {
		//	一定の確率でアイテムを落とす
		if( rand()%20 == 0 ) {
			Item* pItem = new Item();
			pItem->setState(_pos, D3DXVECTOR3(0, 0, -1), SystemCollision::Object::ATT_ITEM, SystemCollision::Object::ATT_PLAYER);
			TASK->addBottom(pItem, SystemTask::LIST_BULLET);
		}
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 4.0f);
		COLLISION->setObject(&_object);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Enemy2::cleanUp(void)
{
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Enemy2::render(void)
{
	// 球の描画
	PRIMITIVE->drawSphere(_pos, 4.0f, _pTexture, 12, _alpha);
}




