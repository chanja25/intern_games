//===========================================================================
//!
//!	@file		Bullet.cpp
//!	@brief		Bulletクラス
//!
//===========================================================================
#include "stdafx.h"



//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Bullet::Bullet(void)
{
}


//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Bullet::~Bullet(void)
{
}


//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	Bullet::init(void)
{
	//	テクスチャーの読み込み
	if( _object.getHitAttribute()==SystemCollision::Object::ATT_PLAYER )
		_pTexture = RESOURCE->loadTexture("Bullet2.bmp");
	else
		_pTexture = RESOURCE->loadTexture("Bullet.bmp");

	// 弾の初期設定
	_oldPos = _pos;

	_life = 1;

	_node.setObject(this);

	_attribute = ATT_UPDATA | ATT_RENDER;
	_state = STATE_RUNNING;
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	Bullet::upDate(void)
{
	//	弾の移動
	_oldPos	= _pos;
	_pos += _vec * _speed;

	//	敵に当たっていれば弾は消える
	if( _object.getState() || (_pos.z > 60) || (_pos.z < -10) ) {
		_state = STATE_INVALID;
	} else {
		_object.setObject(_pos, 0.3f, _oldPos);
		COLLISION->setObject(&_object);
	}
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Bullet::cleanUp(void)
{
	//	テクスチャーの解放
	RESOURCE->releaseTexture(_pTexture);
}

//---------------------------------------------------------------------------
//	描画
//---------------------------------------------------------------------------
void	Bullet::render(void)
{
	// 弾の描画
	PRIMITIVE->drawSphere(_pos, 0.3f, _pTexture, 6);
}

//---------------------------------------------------------------------------
//	弾のステータス設定
//!	@param	pos			 [in]	弾の発射位置
//!	@param	attribute	 [in]	弾の属性
//!	@param	hitAttribute [in]	弾の当たり属性
//---------------------------------------------------------------------------
void	Bullet::setState(D3DXVECTOR3 pos, D3DXVECTOR3 vec, u32 attribute, u32 hitAttribute)
{
	_pos = pos;
	D3DXVec3Normalize(&_vec, &vec);
	_speed = 0.5f;

	_object.init(SystemCollision::Object::SHAPE_CAPSULE, attribute, hitAttribute);
}
