//===========================================================================
//!
//!	@file		DebugCamera.cpp
//!	@brief		DebugCameraクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
//	インスタンスの生成
//---------------------------------------------------------------------------
DebugCamera	DebugCamera::_instance;

//---------------------------------------------------------------------------
//	コンストラクタ
//---------------------------------------------------------------------------
DebugCamera::DebugCamera(void)
{
}

//---------------------------------------------------------------------------
//	デストラクタ
//---------------------------------------------------------------------------
DebugCamera::~DebugCamera(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//---------------------------------------------------------------------------
void	DebugCamera::init(void)
{
	_pos	= D3DXVECTOR3(0, 3, -10);
	_target	= D3DXVECTOR3(0, 0, 0);
	_vec	= D3DXVECTOR3(0, 0, 0);

	upDate();
}

//---------------------------------------------------------------------------
//	更新
//---------------------------------------------------------------------------
void	DebugCamera::upDate(void)
{
	// カメラのリセット
	if( IInput->isButtonPush(0, XInput::BUTTON_RIGHT_THUMB) ) {
		reset();
	}

	// 右スティックの状態取得
	D3DXVECTOR2 thumb = IInput->getRightThumb(0);

	// 敵とプレイヤーのポジションの差を取る
	D3DXVECTOR3 dist = _target - _pos;

	// 左スティックが押されていれば縮小拡大、押されていなければ回転
	if( IInput->isButtonPress(0, XInput::BUTTON_LEFT_THUMB) ) {
		// カメラとターゲットの向きから距離を変えていく
		D3DXVECTOR3 vec = dist;
		D3DXVec3Normalize(&vec, &vec);
		_pos += vec * 0.1f * thumb.x;
	}else {
		// 右スティックの値によって回転行列を作り回転させる
		D3DXMATRIX rot;
		D3DXMatrixRotationYawPitchRoll(&rot, D3DX_PI/180 * thumb.x, D3DX_PI/180 * thumb.y, 0);
		D3DXVec3TransformNormal(&dist, &dist, &rot);

		_pos = _target - dist;
	}

	//	左ステックの値で並行移動させる
	thumb = IInput->getLeftThumb(0);

	D3DXVec3Normalize(&dist, &dist);
	dist *= 0.2f;
	dist.y = 0;
	_vec.x = dist.z;
	_vec.z = -dist.x;

	_pos	+= _vec * thumb.x;
	_target += _vec * thumb.x;
	_pos	+= dist * thumb.y;
	_target += dist * thumb.y;

	
	PRIMITIVE->getShader()->setLightPos(_pos);

	//---------------------------------------------------------------------------
	// ワールド,　ビュー座標の変換　
	//---------------------------------------------------------------------------
	D3DXMatrixLookAtLH( &_view,
						&_pos,
						&_target,
						&D3DXVECTOR3(0, 1, 0) );
	DIRECTX->getD3DDevice()->SetTransform(D3DTS_VIEW, &_view);

	//---------------------------------------------------------------------------
	// ビュー、スクリーン座標の変換
	//---------------------------------------------------------------------------
	D3DXMatrixPerspectiveFovLH( &_projection,
								60.0f*D3DX_PI/180.0f,
								1.0f,
								0.01f,
								1000.0f );
	DIRECTX->getD3DDevice()->SetTransform(D3DTS_PROJECTION, &_projection);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	DebugCamera::cleanUp(void)
{
}

//---------------------------------------------------------------------------
//	デバッグカメラのリセット
//---------------------------------------------------------------------------
void	DebugCamera::reset(void)
{
	_pos	= D3DXVECTOR3(0, 3, -10);
	_target	= D3DXVECTOR3(0, 0, 0);
	_vec	= D3DXVECTOR3(0, 0, 0);
}

//---------------------------------------------------------------------------
//	インスタンス取得
//! @retval デバッグカメラのインスタンス
//---------------------------------------------------------------------------
DebugCamera*	DebugCamera::getInstance(void)
{
	return &_instance;
}
