//===========================================================================
//!
//!	@file		Shader.cpp
//!	@brief		Shaderクラス
//!
//===========================================================================
#include "SystemStdafx.h"


//---------------------------------------------------------------------------
// コンストラクタ
//---------------------------------------------------------------------------
Shader::Shader(void) :
_pEffect(NULL)
{
}

//---------------------------------------------------------------------------
// デストラクタ
//---------------------------------------------------------------------------
Shader::~Shader(void)
{
}

//---------------------------------------------------------------------------
//	初期化
//!	@param	pFileName	[in] ファイル名
//! @retval	TRUE	成功
//! @retval	FALSE	失敗
//---------------------------------------------------------------------------
b32		Shader::init(LPSTR pFileName)
{
	HRESULT hr;
	LPD3DXBUFFER pErr = NULL;

	// シェーダーの読み込み
	hr = D3DXCreateEffectFromFile(	DIRECTX->getD3DDevice(),
									pFileName,
									NULL,
									NULL,
									0,
									NULL,
									&_pEffect,
									&pErr );

	if( FAILED(hr) ) {
		// 読み込み失敗の時、シェーダープログラムのエラー内容を表示
		MessageBox(NULL, (LPCTSTR)pErr->GetBufferPointer(), "エラー(Shader)", MB_OK);
	} else {
		// シェーダーの中のデーターを関連付けする
		_technique	= _pEffect->GetTechniqueByName("TShader");
		_mWVP		= _pEffect->GetParameterByName(NULL, "mWVP");
		_mWorld		= _pEffect->GetParameterByName(NULL, "mWorld");
		_texture	= _pEffect->GetParameterByName(NULL, "Tex");
		_lightDir	= _pEffect->GetParameterByName(NULL, "vLightDir");
		_lightPos	= _pEffect->GetParameterByName(NULL, "vLightPos");
	};

	// ライトの向き
	_light			= D3DXVECTOR4(0.3f, -0.3f, 0.5f, 1.0f);
	// ライトの位置
	_lightPosition	= D3DXVECTOR4(0.0f,  2.0f, 4.0f, 1.0f);
	_pass = 0;

	SAFE_RELEASE(pErr);
	return	SUCCEEDED(hr);
}

//---------------------------------------------------------------------------
//	解放
//---------------------------------------------------------------------------
void	Shader::cleanUp(void)
{
	// エフェクトの解放
	SAFE_RELEASE(_pEffect);
}

//---------------------------------------------------------------------------
//	シェーダーの開始設定
//---------------------------------------------------------------------------
void	Shader::beginShader(void)
{
	if( _pEffect ) {
		_pEffect->SetTechnique(_technique);
		_pEffect->Begin(NULL, 0);
		_pEffect->BeginPass(_pass);
	}

}

//---------------------------------------------------------------------------
//	シェーダーの終了設定
//---------------------------------------------------------------------------
void	Shader::endShader(void)
{
	if( _pEffect ) {
		_pEffect->EndPass();
		_pEffect->End();
	}
}

//---------------------------------------------------------------------------
//	テクスチャーの設定
//!	@param	pTexture	[in] セットするテクスチャー
//---------------------------------------------------------------------------
void	Shader::setTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	if( !_pEffect ) return;
	_pEffect->SetTexture(_texture, pTexture);
}

//---------------------------------------------------------------------------
//	マトリックスの設定
//!	@param	world	[in] ワールド変換マトリックス
//---------------------------------------------------------------------------
void	Shader::setMatrix(const D3DXMATRIX& world)
{
	if( !_pEffect ) return;
	//	変換行列の作成
	LPDIRECT3DDEVICE9 pDevice = DIRECTX->getD3DDevice();
	D3DXMATRIX	mat, view, projection;
	view		= CAMERA->getView();
	projection	= CAMERA->getProjection();

	mat = world * view * projection;

	//	マトリックスの設定
	_pEffect->SetMatrix(_mWVP, &mat);
	//	マトリックスの設定
	_pEffect->SetMatrix(_mWorld, &world);

	//	ライトの設定
	_pEffect->SetVector(_lightDir, &_light);
	//	ライトの設定
	_pEffect->SetVector(_lightPos, &_lightPosition);
}

//---------------------------------------------------------------------------
//	ライトの設定
//!	@param	lightDir	[in] ライトの向き
//---------------------------------------------------------------------------
void	Shader::setLightDir(const D3DXVECTOR3& lightDir)
{
	if( !_pEffect ) return;
	_light.x = lightDir.x;
	_light.y = lightDir.y;
	_light.z = lightDir.z;
	_light.w = 1.0f;
}

//---------------------------------------------------------------------------
//	ライトの設定
//!	@param	lightPos	[in] ライトの位置
//---------------------------------------------------------------------------
void	Shader::setLightPos(const D3DXVECTOR3& lightPos)
{
	if( !_pEffect ) return;
	_lightPosition.x = lightPos.x;
	_lightPosition.y = lightPos.y;
	_lightPosition.z = lightPos.z;
	_lightPosition.w = 1.0f;
}

//---------------------------------------------------------------------------
//	ライトの設定
//!	@param	alpha	[in] アルファ値
//---------------------------------------------------------------------------
void	Shader::setAlpha(f32 alpha)
{
	if( !_pEffect ) return;
	_pEffect->SetFloat("alpha", alpha);
}