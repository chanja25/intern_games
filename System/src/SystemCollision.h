//===========================================================================
//!
//!	@file		SystemCollision.h
//!	@brief		SystemCollisionクラス
//!
//===========================================================================
#pragma once


//===========================================================================
// SystemCollisionクラス
//===========================================================================
class SystemCollision
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//!	初期化
	void	init(void);
	//!	更新
	void	upDate(void);
	//!	解放
	void	cleanUp(void);
	//!@}


	//-------------------------------------------------------------
	//!	@name 当たり判定
	//-------------------------------------------------------------
	//!@{
	//!	球と球の当たり判定
	b32		collision(D3DXVECTOR3 pos1, f32 radius1, D3DXVECTOR3 pos2, f32 radius2);
	//!	球とカプセルの当たり判定
	b32		collisionCapsule(D3DXVECTOR3 pos, f32 radius1, D3DXVECTOR3 pos1, D3DXVECTOR3 pos2, f32 radius2);
	//!@}

	//!	インスタンスの取得
	static	SystemCollision*	getInstance(void);

	//===========================================================================
	//!	Objectクラス(インナークラス)
	//===========================================================================
	class Object {
	public:
		//-------------------------------------------------------------
		//!	@name 初期化
		//-------------------------------------------------------------
		//!@{
		//!	コンストラクタ
		Object(void);
		//!	デストラクタ
		~Object(void);
		//!@}

		//!	当たりを取る形
		enum SHAPE {
			SHAPE_SPHERE,
			SHAPE_CAPSULE,
		};

		//!	当たりの属性
		enum ATTRIBUTE {
			ATT_NONE			= 0,
			ATT_PLAYER			= (1<<0),
			ATT_ENEMY			= (1<<1),
			ATT_PLAYER_BULLET	= (1<<2),
			ATT_ENEMY_BULLET	= (1<<3),
			ATT_ENEMY_BULLET2	= (1<<4),
			ATT_ITEM			= (1<<5),
			ATT_BOSS			= (1<<6),
			ATT_BOSS_BULLET		= (1<<7),
		};

		//!	初期設定
		void	init(u32 shape, u32 attribute, u32 _hitAttribute = 0);
		//!	オブジェクト当たりのセット
		void	setObject(D3DXVECTOR3 pos, f32 radius, D3DXVECTOR3 pos2 = D3DXVECTOR3(0, 0, 0));

		//-------------------------------------------------------------
		//!	@name 取得、設定
		//-------------------------------------------------------------
		//!@{
		//!	位置の取得
		D3DXVECTOR3	getPos(void);
		//!	位置の取得
		D3DXVECTOR3	getPos2(void);
		//!	半径の取得
		f32			getRadius(void);
		//!	ステータスのセット
		void		setState(u32 state);
		//!	ステータスの取得
		u32			getState(void);
		//!	形の取得
		u32			getShape(void);
		//!	属性の取得
		u32			getAttribute(void);
		//!	属性の取得
		u32			getHitAttribute(void);
		//!	ノードの取得
		Node*		getNode(void);
		//!@}

	private:
		D3DXVECTOR3 _pos;			//!< 位置
		D3DXVECTOR3 _pos2;			//!< カプセル型等の為の予備位置
		f32			_radius;		//!< 半径
		u32			_state;			//!< 状態
		u32			_shape;			//!< 形
		u32			_attribute;		//!< 属性
		u32			_hitAttribute;	//!< 属性

		Node		_node;			//!< リスト管理用のノード
	};

	//!	当たりオブジェクト追加
	void	setObject(Object* pObject);
	//!	オブジェクトの当たり判定
	b32		collision(Object* pObj1, Object* pObj2);

private:
	//	コンストラクタ
	SystemCollision(void);
	//	デストラクタ
	~SystemCollision(void);

	static	SystemCollision	_instance;	//!< 唯一のインスタンス


	List		_objectList;	//!< オブジェクトの当たり用リスト
	List		_attackList;	//!< 攻撃の当たり用リスト
};

//! SYSTEMCOLLISIONの取得関数のマクロ
#define	COLLISION	(SystemCollision::getInstance())