//===========================================================================
//!
//!	@file		DirectX.h
//!	@brief		DirectXクラス
//!
//===========================================================================
#pragma once



//===========================================================================
// DirectXクラス
//===========================================================================
class DirectX
{
public:
	//-------------------------------------------------------------
	//!	@name 初期化
	//-------------------------------------------------------------
	//!@{
	//! DirectX初期化
	HRESULT	init(HWND hWnd);
	//! DirectX解放
	void	cleanUp(void);
	//!@}

	//! 描画開始
	void	beginScene(void);
	//! 描画終了
	void	endScene(void);

	//! Direct3Dデバイスの取得
	LPDIRECT3DDEVICE9 getD3DDevice(void);

	//!	頂点情報作成
	HRESULT		createVertex(LPDIRECT3DVERTEXBUFFER9* ppVB, f32 width, f32 height);

	//!	任意軸回転行列の作成
	void		axisRotationMatrix(D3DXMATRIX* out, const D3DXVECTOR3& axis, f32 angle);

	//! インスタンスの取得
	static		DirectX*	getInstance(void);

private:
	//! コンストラクタ
	DirectX(void);
	//! デストラクタ
	~DirectX(void){};

	static	DirectX			_instance;		//!< 唯一のインスタンス

	s32						_cameraMode;	//!< カメラのモード

	D3DXVECTOR3				_pos;			//!< カメラの位置
	D3DXVECTOR3				_target;		//!< カメラのターゲット

	LPDIRECT3D9				_pDevice;		//!< Direct3D9インターフェイス
	LPDIRECT3DDEVICE9		_pD3DDevice;	//!< Direct3Dデバイス
	D3DPRESENT_PARAMETERS	_d3dpp;			//!< パラメーター
};


//! DirectXの取得関数のマクロ
#define	DIRECTX	(DirectX::getInstance())
